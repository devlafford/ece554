module buffer8(clk, rst_n, din, dout, insert, remove, empty, full);
    parameter BUFFER_DEPTH = 256; // this should always be a power of two

    input clk, rst_n;
    input [7:0] din;
    input insert;
    input remove;
    output [7:0] dout;
    output empty, full;

    reg [$clog2(BUFFER_DEPTH)-1:0] produce;
    reg [$clog2(BUFFER_DEPTH)-1:0] consume;
    reg [7:0] buffer [0: BUFFER_DEPTH-1];
    
    assign dout = buffer[consume];
    assign empty = consume == produce;
    assign full = consume-1 == produce;
    reg [31:0] i;

always_ff @(posedge clk, negedge rst_n) begin
    if (!rst_n) begin 
        produce <= 0;
        consume <= 0;
        // don't need to init buffers
        for (i = 0; i < BUFFER_DEPTH; i = i + 1) begin
            buffer[i] <= 8'b00000000;
        end
    end
    else begin
        if (insert) begin
            buffer[produce] <= din;
            produce <= produce + 1;
        end
        if (remove) begin
            buffer[consume] <= 8'h66;
            consume <= consume + 1;
        end
    end
end

endmodule

