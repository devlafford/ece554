// This module is the same as program_rom except it has two read ports since these are shared
// between the cores and a different data width
module data_rom(clk, rst_n, clear, listen, instr_data, l_index, r_index, l_data_out, r_data_out);

    parameter ADDR_BITS = 10;
    parameter DATA_WIDTH = 16;
    parameter INIT_FILE = 0;
    input clk;
    input rst_n;
    input clear;
    input listen;
    input [63:0] instr_data;

    input [ADDR_BITS-1:0] l_index;
    input [ADDR_BITS-1:0] r_index;
    output [DATA_WIDTH-1:0] l_data_out;
    output [DATA_WIDTH-1:0] r_data_out;


reg [DATA_WIDTH-1:0] romfile [0 : 2**ADDR_BITS-1];
reg [ADDR_BITS-1:0] counter;

// default initialization file
always @(negedge rst_n, posedge clk) begin
    if (~rst_n) begin
        case(INIT_FILE)
            2'b00: $readmemh("sine.mem", romfile);
            2'b01: $readmemh("sine.mem", romfile);
            2'b10: $readmemh("sine.mem", romfile);
            2'b11: $readmemh("sine.mem", romfile);
        endcase
    end
    else if (listen) romfile[counter] <= instr_data;
end

// counter for recording new programs into ROM
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) counter <= 0;
    else if (clear) counter <= 0;
    else if (listen) counter <= counter + 1;
    else counter <= counter;
end

assign l_data_out = romfile[l_index];
assign r_data_out = romfile[r_index];


endmodule

