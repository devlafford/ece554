module dsp_core(
    input clk, rst_n,

    // control signals
    input sample_ready,

    // program rom
    input prog, clear,
    input [63:0] instr_data,

    // data roms
    output [9:0] rom0_index,
    output [9:0] rom1_index,
    output [9:0] rom2_index,
    output [9:0] rom3_index,
    input [15:0] rom0_data,
    input [15:0] rom1_data,
    input [15:0] rom2_data,
    input [15:0] rom3_data,

    // shared memory
    input [31:0] shm_from,
    output [7:0] shm_addr,
    output logic shm_write,
    output [31:0] shm_to,

    // sample
    input [23:0] sample_in,
    output [23:0] sample_out,

    input [11:0] peripheral_in,
    output [4:0] peripheral_sel

);

parameter FIR_DEPTH = 16;

//////////////////////
// PROGRAM ROM & PC //
//////////////////////

reg [8:0] pc;
logic halt;
logic stall;
reg stall_ff;

logic [5:0] opcode;
logic [9:0] dstreg;
logic [5:0] src0, src1;
logic [31:0] imm;
logic src1_or_imm; // 0 - src1 & 1 - imm
logic [2:0] instr_pred;

program_rom program_rom(
    .clk(clk),
    .rst_n(rst_n),
    .clear(clear),
    .listen(prog),
    .instr_data(instr_data),
    .program_counter(pc),
    .instr_data_out({opcode, dstreg, src0, src1, imm, src1_or_imm, instr_pred})
);
always @(posedge clk, negedge rst_n) begin
    if(!rst_n) pc <= 0;             // start pc at 0 on reset
    else if(sample_ready) pc <= 0;  // start the pc over when a new sample has arrived
    else if(halt) pc <= pc; // don't advance pc on instrctions that halt pc
    else if(stall) pc <= (stall_ff) ? pc+1 : pc; // don't advance pc when waiting for data
    else pc <= pc+1;                // otherwise increment pc
end

always_ff @(posedge clk, negedge rst_n) begin
    if(!rst_n) stall_ff <= 0;
    else if(stall) stall_ff <= ~stall_ff;
end

///////////////////////////
// PREDICATION REGISTERS //
///////////////////////////

reg [6:0] predregs;
logic[7:0] preds;
logic predwrite;
logic predwrite_addr;
logic predwrite_data;
assign preds = {1'b1, predregs}; // pred 111 is unconditional yes

always @(posedge clk, negedge rst_n) begin
    if (~rst_n) predregs <= 7'b0000000;
    else if (predwrite) begin
        predregs <= predregs; // fuck latches
        predregs[predwrite_addr] <= predwrite_data;
    end
    else predregs <= predregs;
end

///////////////////
// REGISTER FILE //
///////////////////

logic regwrite;
logic regread0;
logic regread1;
reg [31:0] regwrite_data;
logic [31:0] src0_data;
logic [31:0] src1_data;
wire [31:0] wo_regs[960];



register_file regfile(
    .clk(clk),
    .rst_n(rst_n),
    .regwrite(regwrite),
    .regwrite_addrs(dstreg),
    .regwrite_data(regwrite_data),
    .regread0(regread0),
    .regread1(regread1),
    .src0_addrs(src0),
    .src0_data(src0_data),
    .src1_addrs(src1),
    .src1_data(src1_data),
    .sample_ready(sample_ready),
    .sample_out(sample_out),    // register zero is grabbed on every sample interrupt
    .wo_regs(wo_regs)           // write-only regs
);

////////////////////////
// INSTRUCTION DECODE //
////////////////////////

// functional unit interface
logic [31:0] operand0, operand1;
assign operand0 = src0_data;
assign operand1 = src1_or_imm ? imm : src1_data;

// Shared memory assigns
assign shm_addr = operand1[7:0];
assign shm_to = src0_data;

// Peripheral reading assign
assign peripheral_sel = operand1[4:0];

// FU outputs
logic [31:0] alu_out;
logic cmp_out;
logic [31:0] fir_out;
logic [15:0] sine_out;
logic [23:0] hbuf_out;
logic [23:0] processed_hbuf_out;

// instruction decode
// TODO: beware, big trash inbound
always_comb begin

    // default: NOP
    regwrite = 0;
    regwrite_data = 0;
    regread0 = 0;
    regread1 = 0;
    predwrite = 0;
    predwrite_addr = 0;
    predwrite_data = 0;
    shm_write = 0;
    halt = 0;
    stall = 0;

    // check predication
    if (preds[instr_pred]) begin
        casez(opcode)
            // NOP
            6'b000000: begin
                // nothing
            end

            // ALU
            6'b00????: begin
                regwrite = 1;
                regwrite_data = alu_out;
                regread0 = 1;
                regread1 = ~src1_or_imm;
            end

            // CMP
            6'b01????: begin
                regread0 = 1;
                regread1 = ~src1_or_imm;
                predwrite = 1;
                predwrite_addr = dstreg[2:0];
                predwrite_data = cmp_out;
            end

            // READ OSC
            6'b100000: begin
                regwrite = 1;
                regwrite_data = {8'h00, sine_out, 8'h00};
            end

            // SET OSC
            // below with fcws
            6'b100001: begin
                regread1 = ~src1_or_imm;
            end

            // READ FIR
            6'b100010: begin
                regwrite = 1;
                regwrite_data = fir_out;
            end

            // PUT FIR is below
            // below in FIR
            6'b100011: begin
                regread1 = ~src1_or_imm;
            end

            // INPUT HISTORY BUFFER LOAD
            6'b110000: begin
                regwrite = 1;
                regwrite_data = {hbuf_out, 8'h00};
                regread1 = ~src1_or_imm;
                stall = 1;
            end

            // PROCESSED HISTORY BUFFER LOAD
            6'b110001: begin
                regwrite = 1;
                regwrite_data = {processed_hbuf_out, 8'h00};
                regread1 = ~src1_or_imm;
                stall = 1;
            end

            // MOVE
            6'b110010: begin
                regwrite = 1;
                regwrite_data = operand1;
                regread1 = ~src1_or_imm;
            end

            // SHARED MEMORY LOAD
            6'b110100: begin
                regwrite = 1;
                regwrite_data = shm_from;
                regread1 = ~src1_or_imm;
            end

            // SHARED MEMORY STORE
            6'b110101: begin
                regread0 = 1;
                regread1 = ~src1_or_imm;
                shm_write = 1'b1;
            end

            // READ PERIPHERAL
            6'b110110: begin
                regwrite = 1;
                regwrite_data = {12'h00000, peripheral_in, 8'h00};
                regread1 = ~src1_or_imm;
            end
            // BRANCH
            // LABEL

            // HALT
            6'b111110: begin
                halt = 1;
            end

            default: begin // illegal instruction
                // TODO: error condition
            end


        endcase
    end

end


//////////////////////
// FUNCTIONAL UNITS //
//////////////////////

alu fu_alu(
    .op(opcode),
    .in0(operand0),
    .in1(operand1),
    .out(alu_out)
);

compare fu_compare(
    .clk(clk),
    .rst_n(rst_n),
    .opcode(opcode),
    .operand0(operand0),
    .operand1(operand1),
    .result(cmp_out)
);


fir #(
    .RADIX_POS(8),
    .WIDTH(32),
    .DEPTH(16)
    )fu_fir(
    .clk(clk),
    .rst_n(rst_n),
    .put(opcode == 6'b100011),
    .clear(1'b0), //TODO need this?
    .coeffs(wo_regs[64-64:79-64]),
    .new_sample(operand1),
    .product(fir_out),
    .last_sample()
);

/*
sine fu_sine(
    .clk(clk),
    .rst_n(rst_n),
    .fcw_0(wo_regs[128-64][31:8]),
    .fcw_1(wo_regs[129-64][31:8]),
    .fcw_2(wo_regs[130-64][31:8]),
    .fcw_3(wo_regs[131-64][31:8]),
    .rom0_index(rom0_index),
    .rom1_index(rom1_index),
    .rom2_index(rom2_index),
    .rom3_index(rom3_index),
    .rom0_data(rom0_data),
    .rom1_data(rom1_data),
    .rom2_data(rom2_data),
    .rom3_data(rom3_data),
    .osc_select(src0[1:0]),     // src0 determines which osc to read
    .sinus(sine_out)
);*/
sine_modular #(
    .SIZE(2)    //2**SIZE is number of things
    )fu_sine(
    .clk(clk),
    .rst_n(rst_n),
    .fcw('{ wo_regs[128-64][31:8],
            wo_regs[129-64][31:8],
            wo_regs[130-64][31:8],
            wo_regs[131-64][31:8]}),
    .rom_index('{rom0_index,
                 rom1_index,
                 rom2_index,
                 rom3_index}),
    .rom_data('{rom0_data,
                rom1_data,
                rom2_data,
                rom3_data}),
    .osc_select(src0[1:0]),     // src0 determines which osc to read
    .sinus(sine_out)
);

/////////////////////
// HISTORY BUFFERS //
/////////////////////

history_buffer_M10K_ram input_hbuf (
    .clk(clk),
    .rst_n(rst_n),
    .index(operand1[23:8]),     // only use 16b of operand1
    .sample_in(sample_in),
    .sample_ready(sample_ready),
    .sample_out(hbuf_out)
);

/*
history_buffer_M10K_ram processed_hbuf (
    .clk(clk),
    .rst_n(rst_n),
    .index(operand1[23:8]),     // only use 24 MSB of operand1
    .sample_in(sample_out),
    .sample_ready(sample_ready),
    .sample_out(processed_hbuf_out)
);
*/


endmodule
