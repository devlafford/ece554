// This module is the same as program_rom except it has two read ports since these are shared
// between the cores and a different data width
module data_rom_M10K(clk, rst_n, clear, listen, instr_data, l_index, r_index, l_data_out, r_data_out);

    parameter ADDR_BITS = 10;
    parameter DATA_WIDTH = 16;
    parameter INIT_FILE = "../../verilog/sine.mif";
    input clk;
    input rst_n;
    input clear;
    input listen;
    input [63:0] instr_data;

    input [ADDR_BITS-1:0] l_index;
    input [ADDR_BITS-1:0] r_index;
    output [DATA_WIDTH-1:0] l_data_out;
    output [DATA_WIDTH-1:0] r_data_out;


reg [ADDR_BITS-1:0] counter;

ram_16_1K rom (.address_a(l_index), .address_b(r_index), .clock(clk),
			   .data_a(15'h0000), .data_b(15'h0000), .wren_a(1'b0), .wren_b(1'b0),
			   .q_a(l_data_out), .q_b(r_data_out));
defparam rom.INIT_DATA = INIT_FILE;

// counter for recording new programs into ROM
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) counter <= 0;
    else if (clear) counter <= 0;
    else if (listen) counter <= counter + 1;
    else counter <= counter;
end
endmodule
