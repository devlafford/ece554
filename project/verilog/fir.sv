module fir(clk, rst_n, put, clear, coeffs, new_sample, product, last_sample);

parameter RADIX_POS = 0;
parameter WIDTH = 32;
parameter DEPTH = 16;

input clk, rst_n, put, clear;
input [WIDTH-1:0] coeffs[DEPTH];
input [WIDTH-1:0] new_sample;
output logic [WIDTH-1:0] product;
output [WIDTH-1:0] last_sample;

logic [WIDTH-1:0] samples[DEPTH];
logic [WIDTH-1:0] cproducts[DEPTH];

assign last_sample = samples[DEPTH-1];

generate
genvar i;

    for(i=1;i<DEPTH; i+=1) begin : FIR_BLOCK1
        always @(posedge clk, negedge rst_n) begin
            if(!rst_n) samples[i] <= 0;
            else if(clear) samples[i] <= 0;
            else if(put) samples[i] <= samples[i-1];
            else samples[i] <= samples[i];
        end
    end

    for(i=0;i<DEPTH; i+=1) begin : FIR_BLOCK2
        multiplier m( 
            .A(samples[i]),
            .B(coeffs[i]), 
            .Product(cproducts[i])
        );
    end

endgenerate
always @(posedge clk, negedge rst_n) begin 
    if(!rst_n) samples[0] <= 0;
    else if(clear) samples[0] <= 0;
    else if(put) samples[0] <= new_sample;
    else samples[0] <= samples[0];
end

integer j;
    always_comb begin
	product = 0;
        for(j=0;j<DEPTH; j+=1) product = product + cproducts[j];
    end


endmodule

