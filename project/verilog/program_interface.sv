// TODO: add responses for bad headers and bad instructions
module program_interface(
    input clk,
    input rst_n,
    output txd,
    input rxd,

    output reg receiving_program,
    output reg new_program,
    output reg lprog,               // determines if left core should listen
    output reg rprog,               // determines if right core should listen
    output reg [2:0] romprog,       // 3'b111 is don't write
    output reg [63:0] instr_data
);

/* --- STATE MACHINE --- */
typedef enum reg [1:0] {IDLE, DATA, LOOP, CHECK} state_t;
state_t state, next_state;

logic spart_iocs;
logic spart_iorw;
wire spart_rda;
wire spart_tbr;
wire [7:0] spart_databus;

spart sprt(
    .clk(clk),
    .rst(!rst_n),
    .iocs(spart_iocs),  // send the command
    .iorw(spart_iorw),  // read or write command
    .rda(spart_rda),    // indicates that a new program is being received; transition
    .ioaddr(2'b0),         // never reading the status reg or setting baud
    .databus(spart_databus),
    .txd(txd),
    .rxd(rxd)
);

/* --- INSTRUCTION RECEIVE --- */
reg [2:0] recv_count;
logic reset_counter, inc_count;
always_ff @(posedge clk, negedge rst_n) begin
    if (!rst_n) recv_count <= 0;
    else if (reset_counter) recv_count <= 0;
    else if (inc_count) recv_count <= recv_count + 1;
    else recv_count <= recv_count;
end

logic shift_in;
reg [7:0] data_tmp;
logic save_data;
always_ff @(posedge clk, negedge rst_n) begin
    if (!rst_n) instr_data <= 0;
    else if (shift_in) begin
		instr_data <= {data_tmp, instr_data[63:8]};
    end
end


always_ff @(posedge clk, negedge rst_n) begin
    if (!rst_n) data_tmp <= 0;
    else if (save_data) begin
		data_tmp <= spart_databus;
    end
end

/* --- DESTINATION --- */
typedef enum reg [2:0] {
    NULL,       // no hardware listens to null; throw bytes following a null or bad header away
    LCHANNEL,
    RCHANNEL,
    ROM0,
    ROM1,
    ROM2,
    ROM3
} destination_t;
destination_t destination, next_destination;

/* --- LISTEN CONTROL SIGNALS --- */
always_ff @(posedge clk, negedge rst_n)
	if(!rst_n) destination <= NULL;
	else destination <= next_destination;

assign {lprog, rprog, romprog} =    (instr_data[63:16] == 48'hFFFFFFFFFFFF) ? 5'b00111 :    // don't write section headers to ROMs
                                    (state != CHECK)                        ? 5'b00111 :    // don't write until after we have all 8 bytes
                                    (destination == LCHANNEL)               ? 5'b10111 : 
                                    (destination == RCHANNEL)               ? 5'b01111 : 
                                    (destination == ROM0)                   ? 5'b00000 : 
                                    (destination == ROM1)                   ? 5'b00001 : 
                                    (destination == ROM2)                   ? 5'b00010 : 
                                    (destination == ROM3)                   ? 5'b00011 : 
                                                                             5'b00111;     // destination is NULL

// state machine ff
always_ff @(posedge clk, negedge rst_n) begin
    if (!rst_n) state <= IDLE;
    else state <= next_state;
end

// state machine comb
always_comb begin
    next_state = state;
	
	spart_iocs = 0;
	spart_iorw = 0;
	
	receiving_program = 0;
    new_program = 0;
	
	reset_counter = 0;
	inc_count = 0;
	shift_in = 0;
	save_data = 0;

	next_destination = destination;
	
    case(state) 
        IDLE: begin
            if (spart_rda) begin
                new_program = 1;
				next_state = DATA;
            end
            else next_state = IDLE;
        end

        DATA: begin
            receiving_program = 1;
			if(spart_rda) begin
				spart_iocs = 1;
				spart_iorw = 1;
				save_data = 1; // Save data to temporary reg
				next_state = LOOP;
			end
			else	
				next_state = DATA;
        end
		
		LOOP: begin // Allow spart_rda to update
			receiving_program = 1;	
			if(!spart_rda) begin
				inc_count = 1;
				shift_in = 1; // shift in most recent data on falling edge of rda
				if (recv_count == 7)
					next_state = CHECK;
				else
					next_state = DATA;
			end
			else begin
				spart_iocs = 1;
				spart_iorw = 1;
				save_data = 1; // Save to temporary reg
				next_state = LOOP; 
			end
		end

		
		CHECK: begin
			receiving_program = 1;

			// Set next destination //
			if(&instr_data[63:16]) begin // Is a header
				case (instr_data[15:0])
					16'h0000: next_destination = LCHANNEL;
					16'h0001: next_destination = RCHANNEL;
					16'h0100: next_destination = ROM0;
					16'h0101: next_destination = ROM1;
					16'h0102: next_destination = ROM2;
					16'h0103: next_destination = ROM3;
					default: next_destination = NULL;			
				endcase
			end
			
			// We're done if we've received end header.
			// Second condition lets us leave after receiving an end header
			if (&instr_data | (destination == NULL & next_destination == NULL)) begin
				next_state = IDLE;
			end	
			else 
				next_state = DATA;
		end
    endcase

end



endmodule




