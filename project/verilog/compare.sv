module compare(	input [5:0] opcode,
					input [31:0] operand0, 
					input [31:0] operand1, 
					input clk, 
					input rst_n, 
					output logic result);

//logic [31:0] PredReg [2:0]; 

always_comb begin
    case(opcode)
        6'b010000 : result = ($signed(operand0) >  $signed(operand1) ? (1'b1) : (1'b0));    // gt
        6'b010001 : result = ($signed(operand0) <  $signed(operand1) ? (1'b1) : (1'b0));    // lt
        6'b010010 : result = ($signed(operand0) >= $signed(operand1) ? (1'b1) : (1'b0));    // gte
        6'b010011 : result = ($signed(operand0) <= $signed(operand1) ? (1'b1) : (1'b0));    // lte
        6'b010100 : result = ($signed(operand0) == $signed(operand1) ? (1'b1) : (1'b0));    // eq
        6'b010101 : result = ($signed(operand0) != $signed(operand1) ? (1'b1) : (1'b0));    // neq
        6'b010110 : result = (( operand1 == 32'h7fffffff ) | ( operand1 == 32'h80000000 )) ? 1'b1 : 1'b0; // saturation

        default: result = 1'b0;


    endcase
end
	



endmodule
