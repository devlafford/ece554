module control (
    output [15:0] debugs,
    input clk,
    input rst_n,

    // 5 wire interface
    input adcdat,
    output dacdat,
    output frame_sync,
    output bclk,
    output xclk,

    // spart
    output spart_tx,
    input spart_rx,

    // i2c
    input src_toggle,
    output i2c_sclk,
    output i2c_sda,

    // core interface
    output [23:0] lsample_to_core,
    output [23:0] rsample_to_core,
    input [23:0] lsample_from_core,
    input [23:0] rsample_from_core,
    output reg sample_out,

    output new_program,         // reset core roms
    output lprog,               // left channel rom listen
    output rprog,               // right channel rom listen
    output [2:0] romprog,       // other rom listen
    output [63:0] instr_data   // instruction/data

);

/* --- DECLARATIONS --- */
reg [15:0] i2c_data;
reg i2c_transmit, i2c_ready;

typedef enum reg [2:0] {INIT, CONFIG, WAIT, TXRX, PROGRAM} state_t;
state_t state, next_state;
wire begin_sample;
 
/* --- I2C CONFIGURATION --- */
// adc source selection
reg adc_src; // 0 - line in, 1 - microphone
reg src_toggled;
reg should_toggle_src; // srff for state machine
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) adc_src <= 0;
    else if (src_toggled) adc_src <= ~adc_src;
    else adc_src <= adc_src;
end
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) should_toggle_src <= 0;
    else if (src_toggle) should_toggle_src <= 1;
    else if (src_toggled) should_toggle_src <= 0;
    else should_toggle_src <= should_toggle_src;
end


// TODO: add muting to these commands in the beginning for the case where we are 
//       changing the audio source without a reset. 
// i2c configuration commands
reg [3:0] command_index;
logic [7:0] wm8731_addr = 8'b0011_0100;
logic [3:0] command_count = 4'd10;
logic [15:0] commands_line_in[10] =
     '{16'b00001111_00000000,               // Reset Device
     16'b00001111_00000000,                 // Reset Device
     16'b0000000_000010111,                 // Left Line In (default but no mute)
     16'b0000001_000010111,                 // Right Line In (default but no mute)
     16'b0000100_000110001,                 // Analogue Audio Path Control (select mic and dac to adc)
     16'b0000101_000000000,                 // Digital Audio Path Control (unmute DAC)
     16'b0000111_000001011,                 // Digital Audio Interface (DSP mode, 24 bit data, defaults)
     16'b0001000_000011100,                 // BOSR
     16'b0001001_000000001,                 // Active Control (set active bit)
     16'b0000110_000000000};                // Power Down Control (enable everything but clock and osc)
logic [15:0] commands_mic_in[10] =
     '{16'b0001111_000000000,               // Reset Device
     16'b00001111_00000000,                 // Reset Device
     16'b0000000_000010111,                 // Left Line In (default but no mute)
     16'b0000001_000010111,                 // Right Line In (default but no mute)
     16'b0000100_000010101,                 // Analogue Audio Path Control (select mic and dac to adc)
     16'b0000101_000000000,                 // Digital Audio Path Control (unmute DAC)
     16'b0000111_000001011,                 // Digital Audio Interface (DSP mode, 24 bit data, defaults)
     16'b0001000_000011100,                 // BOSR
     16'b0001001_000000001,                 // Active Control (set active bit)
     16'b0000110_000000000};                // Power Down Control (enable everything but clock and osc)

// i2c command_index ff
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) command_index <= 0;
    else if (src_toggle) command_index <= 0;
    else if (state == CONFIG && i2c_ready) command_index <= command_index + 1;
    else command_index <= command_index;
end

// i2c_master for configuration
i2c_master i2c(
    .clk(clk),
    .rst_n(rst_n),
    .address(wm8731_addr),
    .data(i2c_data),
    .transmit(i2c_transmit),
    .sda(i2c_sda),
    .sclk(i2c_sclk),
    .ready(i2c_ready)
);

/* --- PROGRAM INTERFACE --- */
wire receiving_program;
program_interface piface(
    .clk(clk),
    .rst_n(rst_n),
    .txd(spart_tx),
    .rxd(spart_rx),
    .receiving_program(receiving_program),
    .lprog(lprog),
    .rprog(rprog),
    .romprog(romprog),
    .instr_data(instr_data)
);

/* --- CLOCK CIRCUITRY --- */
// master clock 12.288 MHz
pll_12_228 pll(
    .refclk(clk),
    .rst(~rst_n),
    .outclk_0(xclk)
);

// sample frequency is 96 kHz; 12.288 MHz / 128
reg [8:0] fs_cnt;
always_ff @(posedge xclk, negedge rst_n) begin
    if (~rst_n) fs_cnt <= 0;
    else fs_cnt <= fs_cnt + 1;
end
reg tt, tt1, tt2;
// Double flop for crossing clock domains
always_ff @(posedge clk) begin
    tt <= fs_cnt == 9'b100000000;
    tt1 <= tt;
    tt2 <= tt1;    
end
assign begin_sample = (state == WAIT) && tt2;

// bitclk counter 
// runs at 6.25 MHz; that is fast enough
reg [3:0] bitclk_cnt;
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) bitclk_cnt <= 0;
    else bitclk_cnt <= bitclk_cnt + 1;
end
assign bclk = bitclk_cnt[3];

/* --- SHIFT REG AND COUNTERS --- */
// bit counter
reg [6:0] bit_counter;
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) bit_counter <= 0;
    else if (state == WAIT) bit_counter <= 0;
    else if (bitclk_cnt == 4'b1111) bit_counter <= bit_counter + 1; // shift on falling edge of bclk
    else bit_counter <= bit_counter;
end

// sample shift reg
reg [47:0] shifter;
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) shifter <= 0;
    else if (begin_sample) shifter <= {lsample_from_core, rsample_from_core};
    else if (bitclk_cnt == 4'b1111) shifter <= {shifter[46:0], adcdat};  // shift on falling edge of bclk
    else shifter <= shifter;
end
assign dacdat = (state == PROGRAM) ? 0 : shifter[47]; // don't send audio while we are sending new program

// left right channel samples 
// output the samples once we have new ones ready
assign lsample_to_core = shifter[47:24];
assign rsample_to_core = shifter[23:0];

/* --- STATE MACHINE --- */
// lrck management
assign frame_sync = (state == TXRX) && (bit_counter == 1);

// state machine that configures and then loops getting and playing samples
// state machine ff
always_ff @(posedge clk, negedge rst_n) begin
    if (!rst_n) state <= CONFIG;
    else state <= next_state;
end

// state machine comb
always_comb begin
    next_state = WAIT;
    sample_out = 0;

    i2c_data = 0;
    i2c_transmit = 0; 
    src_toggled = 0;

    case(state)
    INIT: begin // send the initialization commands to WM8731
        if (command_index == command_count) begin // we are finished sending the config commands
            next_state = WAIT;
        end
        else begin // we still have commands to send
            next_state = CONFIG;
            if (adc_src) i2c_data = commands_mic_in[command_index];
            else i2c_data = commands_line_in[command_index];
            i2c_transmit = 1'b1;
        end
    end

    CONFIG: begin // wait for the I2C to be ready again
        if (i2c_ready) next_state = INIT;
        else next_state = CONFIG;
    end

    WAIT: begin
        if (should_toggle_src) begin
            next_state = INIT; // changed the source of the audio and need to resend the commands
            src_toggled = 1;
        end
        else if (receiving_program) next_state = PROGRAM; // SPART has received bytes, so a new program is being sent
        else if (begin_sample) next_state = TXRX;
        else next_state = WAIT;
    end

    TXRX: begin
        if (bit_counter == 48) begin
            next_state = WAIT;
            sample_out = 1;
        end
        else next_state = TXRX;
    end

    PROGRAM: begin
        if (~receiving_program) next_state = WAIT;
        else next_state = PROGRAM;
    end

    endcase
end

assign debugs[2:0] = state;
assign debugs[3] = receiving_program;
assign debugs[4] = new_program;


endmodule
