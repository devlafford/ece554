module program_rom(clk, rst_n, clear, listen, instr_data, program_counter, instr_data_out);

    parameter PC_BITS = 9;
    input clk;
    input rst_n;
    input clear;
    input listen;
    input [63:0] instr_data;

    input [PC_BITS-1:0] program_counter;
    output [63:0] instr_data_out;

reg [63:0] romfile [0 : 2**PC_BITS-1];
reg [PC_BITS-1:0] counter;

// initialize a default program
initial begin
    $readmemh("../verilog/defaultprog.mem", romfile);
end

// counter for recording new programs into ROM
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) counter <= 0;
    else if (clear) counter <= 0;
    else if (listen) counter <= counter + 1;
    else counter <= counter;
end

// romfile
always_ff @(posedge clk) begin
    if (listen) romfile[counter] <= instr_data;
end

assign instr_data_out = romfile[program_counter];


endmodule
