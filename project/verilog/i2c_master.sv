module i2c_master(
    input clk, rst_n,
    input [7:0] address,
    input [15:0] data,
    input transmit,
    output logic sda, sclk,
    output logic ready
);
// in the case of the WM8731:
// address is 00011010 (if CSB state is zero, which it probably is)
// data high is which register to write
// data low is the data to write into that register

reg [27:0] shifter;
reg [4:0] bit_counter;
reg [8:0] sclk_counter;

// state machine
typedef enum reg [1:0] {IDLE, TX, ACK, STOP} state_t;
state_t state, next_state;
reg shift;

// state machine sequential
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) state <= IDLE;
    else state <= next_state;
end

// state machine comb
always_comb begin
    // defaults
    next_state = IDLE;
    ready = 0;

    // transition logic
    case(state)
        IDLE: begin
            ready = 1;
            if (transmit) next_state = TX;
        end

        TX: begin
            if (bit_counter == 9 || bit_counter == 18 || bit_counter == 27) next_state = ACK;
            else next_state = TX;
        end

        ACK: begin
            if (shift && (bit_counter == 27)) next_state = STOP;
            else if (shift) next_state = TX;
            else next_state = ACK;
        end
        STOP: begin
            if (sda & sclk) next_state = IDLE;
            else next_state = STOP;

        end

    endcase

end


// shifter
assign shift = (sclk_counter == 0); // shift on negedge of sclk
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) shifter <= 28'hFFFFFFF;
    else if (transmit) shifter <= {1'b1, address, 1'b1, data[15:8], 1'b1, data[7:0], 1'b1};
    else if (shift) shifter <= {shifter[26:0], 1'b1};
    else shifter <= shifter;
end
assign sda =    (state == ACK) ? 1'bz : // ACK
                (bit_counter == 0 && sclk_counter[7])  ? 1'b0 : // start condition
                (bit_counter == 28 && sclk_counter[7]) ? 1'b1 : // stop condition
                (bit_counter == 28) ? 1'b0 :
                shifter[27];

// bit counter
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) bit_counter <= 0;
    else if (transmit) bit_counter <= 0;
    else if (shift) bit_counter <= bit_counter + 1;
    else bit_counter <= bit_counter;
end

// clock generator
// 9 bit wide divider, resulting in a ~200khz freq
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) sclk_counter <= 9'b100000000;
    else if (transmit) sclk_counter <= 9'b100000000;
    else if (state != IDLE) sclk_counter <= sclk_counter + 1;
    else sclk_counter <= sclk_counter;
end
assign sclk = sclk_counter[8];



endmodule
