module history_buffer_M10K_ram(clk, rst_n, index, sample_in, sample_ready, sample_out);

    parameter ADDR_WIDTH = 0;
    input clk, rst_n;
    input [15:0] index;
    input [23:0] sample_in;
    input sample_ready;
    output [23:0] sample_out;

// use this as a circular buffer
reg [15:0] head;
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) head <= 0;
    else if (sample_ready) head <= head + 1;
    else head <= head;
end

reg [15:0] tail;
assign tail = head - index;

// first shift reg
ram_65536 ram (
    .address(sample_ready ? head + 1 : tail),
    .clock(clk),
    .data(sample_in),
    .wren(sample_ready),
    .q(sample_out)
);

endmodule
