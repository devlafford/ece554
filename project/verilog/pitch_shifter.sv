module pitch_shifter(input clk,
		              input rst,
		                        input readPitchShifter,         //grabs an output from the buffer
		                        input signed [23:0] delta,      //determines magnitude of pitch shift
		              input [23:0] sampleIn,          //new sample to be processed
		    input putPitchShifter,         insert2,
		            remove1,
		    remove2;


		    logic pitchDirection;  assign pitchDirection = (delta < 0);
		    /*Current reads from the buffers*/
		    logic [23:0] buffer1Out;  logic [23:0] counterTarget;  assign counterTarget = pitchDirection ? -delta - 1: delta;
		    /*Two circular buffers used to implement the pitch shifting effect*/
		    cBufferShifter #(256, 0) cBufferShifter(.clk(clk),
							                                             .rst_n(~rst),
							                                             .din(sampleIn),
							                                             .dout(buffer1Out),
							                                             .delta(delta),
							                                             .insert(insert1),
							                                             .remove(remove1),
							                                             .empty(),
							    .full());

		      cBufferShifter #(256, 128) cBufferShifterOffset(.clk(clk),
								                                                       .rst_n(~rst),
								                                                       .din(sampleIn),
								                                                       .delta(delta),
								                                                       .dout(buffer2Out),
								                                                       .insert(insert2),
								                                                       .remove(remove2),
								                                                       .empty(),
								      .full());


		      /*Logic for buffer counter.*/
		    logic countComplete;  assign countComplete = (bufferCounter == counterTarget);  assign remove1 = ((countComplete && pitchDirection && readPitchShifter) || (!pitchDirection && readPitchShifter)) ? 1'b1 : 1'b0;  assign remove2 = ((countComplete && pitchDirection && readPitchShifter) || (!pitchDirection && readPitchShifter)) ? 1'b1 : 1'b0;  assign insert1 = putPitchShifter ? 1'b1 : 1'b0;  assign insert2 = putPitchShifter ? 1'b1 : 1'b0;  always_ff @(posedge clk) bufferCounter <= (countComplete || rst || !readPitchShifter) ? 32'h0 : bufferCounter + 1'b1;
		    /*Output is the average of the outputs of the two circular buffers.*/
		    assign sampleOut = pitchDirection ? (buffer1Out >>> 1) + (buffer2Out >>> 1) : buffer1Out;
		    endmodule
