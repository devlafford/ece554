module sine_modular(clk, rst_n, fcw, rom_index, rom_data, osc_select, sinus);
	parameter SIZE = 2;
	
	input clk, rst_n;
	input [23:0] fcw [2**SIZE];
	input [SIZE-1:0] osc_select;
	input [15:0] rom_data [2**SIZE];
	output [9:0] rom_index [2**SIZE];
	output logic signed [15:0] sinus;

	reg [23:0] accu [2**SIZE];
	
	reg [7:0] fdiv_cnt;
	wire accu_en;
	reg accu_msb_q;
	
	//process for frequency divider
	always @(posedge clk, negedge rst_n)
	begin
		if(!rst_n) fdiv_cnt <= 0;
		else if(accu_en == 1'b1) fdiv_cnt <= 0;
		else fdiv_cnt <= fdiv_cnt + 1;
	end
	
	//logic for accu enable signal, resets also the frequency divider counter
	assign accu_en = (fdiv_cnt == 8'b10000000) ? 1'b1 : 1'b0;
	
	generate
	genvar i;
	for(i = 0; i < 2**SIZE; i = i + 1) begin : ACCUM
		always @(posedge clk, negedge rst_n)
		if(!rst_n)
			accu[i] <= 0;
		else if(accu_en)
				accu[i] <= accu[i] + fcw[i];
		
		assign rom_index[i] = accu[i][23:14];
	end
	endgenerate
	
	assign sinus = rom_data[osc_select];
	
endmodule
