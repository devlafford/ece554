module delay(
    input clk, rst_n,
    input sample_ready,
    inout [23:0] lsample,
    inout [23:0] rsample,
    input enable

);

parameter FLANGE_MAX = 400;
parameter FLANGE_MIN = 50;
parameter ADDR_WIDTH = 16;
reg [ADDR_WIDTH-1 : 0] flange;

// storage for past samples
wire signed [23:0] lhistout;
wire signed [23:0] rhistout;
history_buffer_M10K_ram #(
    .ADDR_WIDTH(ADDR_WIDTH)
    ) 
    lhist(
    .clk(clk),
    .rst_n(rst_n),
    .index(65000),
    .sample_in(lsample),
    .sample_ready(sample_ready),
    .sample_out(lhistout)
);
history_buffer_M10K_ram #(
    .ADDR_WIDTH(ADDR_WIDTH)
    ) 
    rhist(
    .clk(clk),
    .rst_n(rst_n),
    .index(65000),
    .sample_in(rsample),
    .sample_ready(sample_ready),
    .sample_out(rhistout)
);

// storage for current sample
reg signed [23:0] lcurrent;
reg signed [23:0] rcurrent;
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) lcurrent <= 0;
    else if (sample_ready) lcurrent <= lsample;
    else lcurrent <= lcurrent;
end
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) rcurrent <= 0;
    else if (sample_ready) rcurrent <= rsample;
    else rcurrent <= rcurrent;
end

reg [9:0] sample_div;
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) sample_div <= 0;
    else if (sample_ready) sample_div <= sample_div + 1;
    else sample_div <= sample_div;
end
reg direction;
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) begin
        direction <= 1'b1;
        flange <= FLANGE_MIN;
    end
    else if (sample_div == 10'hFFF) begin
        if (flange == FLANGE_MAX-1) direction <= 1'b0;
        else if (flange == FLANGE_MIN+1) direction <= 1'b1;
        else direction <= direction;

        if (direction) flange <= flange + 1;
        else flange <= flange - 1;
    end
end

wire signed [31:0] lout = (lhistout + lcurrent);
wire signed [31:0] rout = (rhistout + rcurrent);

assign lsample = sample_ready ? 24'hZZZZZZ : enable ? lout[23:0] : lcurrent;
assign rsample = sample_ready ? 24'hZZZZZZ : enable ? rout[23:0] : rcurrent;




endmodule
