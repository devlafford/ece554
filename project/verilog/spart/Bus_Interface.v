module Bus_Interface (input rda, tbr, iorw, iocs, inout[7:0] databus, input[7:0] rx_buffer, input[1:0] ioaddr,  output reg [7:0] dataout);
	
	// When ioaddr = 2'b10 or 2'b11, the iorw is required to be 1'b0.
	// When iorw = 0, it is always receive data from databus
	// When iorw = 1, it is always send data to databus
	
	
	assign databus = ( iocs & iorw) ? ( ioaddr == 2'b00)? rx_buffer : {6'b000_000, tbr, rda} :8'hzz ;
	always@(*) begin 
		dataout = databus;
	end
	
endmodule 