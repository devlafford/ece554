module BAUD_Rate_Gen( input clk, rst, input [7:0] divisor, input [1:0] ioaddr, output reg enable);


	reg [15:0] divisionBuffer, counter;

	// Load value into Divisor_Buffer.
	
    always @(posedge clk)
		if ( rst )
			divisionBuffer <= 16'h00a2; // 19200 baud
		else if ( ioaddr == 2'b10) 
			divisionBuffer <= {divisionBuffer[15:8],divisor};  //load the divisor (low)
			
		else if (ioaddr == 2'b11)		
			divisionBuffer <= {divisor, divisionBuffer[7:0]}; // load the divisor (high)
		else	
			divisionBuffer <= divisionBuffer;
			
	
	//counter
	// When reset is pressed, counter is initilized to 0
	always @(posedge clk)
		if ( rst ) begin
			counter <= 16'h0000;
			enable <= 0;
		end
		
		//Reload the counter when it reaches zero
		else if (counter == 16'h0000) begin
			counter <= divisionBuffer;
			enable <= 1;
		end
		else begin
			counter <= counter - 1;
			enable <= 0;
		end

endmodule 