module driver(
    input clk,
    input rst,
    input [1:0] br_cfg,
    output reg iocs,
    output reg iorw,
    input rda,
    input tbr,
    output reg [1:0] ioaddr,
    inout [7:0] databus,
	 output reg [7:0] gpio_led
    );


	reg [1:0] LOAD_COUNT;
	reg [7:0] data_buffer;
	reg flag;

	assign databus = (iorw) ? 8'hzz : data_buffer;

	always @(posedge clk)
	begin
		if (rst)
		begin
			LOAD_COUNT <= 2'b10;
			iocs <= 1'bx;
			iorw <= 1'bx;
			ioaddr <= 2'bx;
			flag <= 0;
			data_buffer <= 8'hxx;
		end
		// Send Divisor to BAUD_Rate_Gen
		else if (LOAD_COUNT != 2'b00)
		begin
			LOAD_COUNT <= LOAD_COUNT - 1;
			iorw <= 0;
			iocs <= 1;
			if (LOAD_COUNT == 2'b10) //load low
			begin
				ioaddr <= 2'b10;
				case (br_cfg)
					2'b11: data_buffer <= 8'h51; //81   (decimal value of high and load byte combined)
					2'b00: data_buffer <= 8'h8A; //650
					2'b01: data_buffer <= 8'h45; //325
					2'b10: data_buffer <= 8'ha2; //162
				endcase
				flag <= flag;
			end
			else //load high
			begin
				ioaddr <= 2'b11;
				case (br_cfg)
					2'b11: data_buffer <= 8'h00;
					2'b00: data_buffer <= 8'h02;
					2'b01: data_buffer <= 8'h01;
					2'b10: data_buffer <= 8'h00;
				endcase
				flag <= flag;
			end
		end
		else if (rda == 1) //receive data
		begin
			LOAD_COUNT <= 2'b00;
			iocs <= 1;
			iorw <= 1;
			ioaddr <= 2'b00;
			flag <= 1;
			data_buffer <= databus;
		end
		else if ({flag,tbr} == 2'b11) //send data
		begin
			LOAD_COUNT <= 2'b00;
			iocs <= 1;
			iorw <= 0;
			ioaddr <= 2'b00;
			flag <= 0;
			data_buffer <= data_buffer;
		end
		else //idle
		begin
			LOAD_COUNT <= 2'b00;
			iocs <= 1;
			iorw <= 1;
			ioaddr <= 2'b00;
			flag <= flag;
			data_buffer <=data_buffer;
		end
	end
	
	always @(*)
	if (rst)
		gpio_led = {br_cfg, 6'h00};
	else
		gpio_led = data_buffer;

endmodule