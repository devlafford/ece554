module spart(
    input clk,
    input rst,
    input iocs,
    input iorw,
    output rda,
    output tbr,
    input [1:0] ioaddr,
    inout [7:0] databus,
    output txd,
    input rxd
	 //output [7:0] gpio_led
    );

	wire [7:0] Dataout,Rx_Buffer;
	wire Enable;

	SPART_Transmitter transmit(.TxD(txd), .clk(clk), .rst(rst), .Enable(Enable), .TBR(tbr), .DATA(Dataout), .IOADDR(ioaddr), .IORW(iorw), .IOCS(iocs));

	SPART_Receiver receive( .enable(Enable), .clk(clk), .rst(rst), .data(Rx_Buffer), .rda(rda), .rxd(rxd), .iocs(iocs), .iorw(iorw), .ioaddr(ioaddr));
	
	Bus_Interface Bus( .rx_buffer(Rx_Buffer), .rda(rda), .tbr(tbr), .databus(databus), .dataout(Dataout), .iorw(iorw), .ioaddr(ioaddr), .iocs(iocs));

	BAUD_Rate_Gen Baud(.divisor(Dataout), .ioaddr(ioaddr), .clk(clk), .rst(rst), .enable(Enable));

	

endmodule