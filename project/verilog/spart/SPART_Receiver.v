module SPART_Receiver(output reg [7:0] data, output reg rda, input rxd, enable, clk, rst, iocs, iorw, input[1:0] ioaddr);

	reg [8:0] receiveBuffer;
	reg [3:0] counter, signalC;
	
	
	// When the receiver gets the start bit, it sets counter to 10 and starts receiving the following data bits.
	

	always @(posedge clk)
		if ( rst )
			receiveBuffer <= 9'hxxx;
		// Store bit when counter is not 4'h0, enable is 1, and signalC is 4'h7
		else if ({|counter, enable, signalC} == 6'b0011_0111)
			receiveBuffer <= {rxd, receiveBuffer[8:1]};
		else
			receiveBuffer <= receiveBuffer;
	
	always @(posedge clk)
		if ( rst )
			counter <= 4'h0;
		// Receiving start bit, set counter to start receiving data
		else if ({enable, rxd, counter} == 6'b0010_0000)
			counter <= 4'ha;
		// Store one bit, so decreasing counter
		else if ({enable, signalC}== 5'h17)
			counter <= counter - 1;
		else
			counter <= counter;
			
	always @(posedge clk)
		if ( rst )
			signalC <= 4'h0;
		// sample rxd
		else if ( enable ) begin
			// idle status
			if ( counter == 4'h0 )
				signalC <= 4'h0;
			// receiving data
			else
				signalC <= signalC + 1;
		end
		// not sample rxd
		else
			signalC <= signalC;
	always @(*)
		data = receiveBuffer[7:0];	

	always @(posedge clk)
		if (rst)
			rda <= 0;
		else if ( {signalC,counter} == 8'h80 )  // receiving data is finished
			rda <= 1;		
		else if ({iorw,ioaddr} == 3'b100) // data read by the driver
			rda <= 0;
	

endmodule 