module register_file(
    clk, rst_n,
    regwrite,
    regwrite_addrs,
    regwrite_data,
    regread0,
    regread1,
    src0_addrs,
    src0_data,
    src1_addrs,
    src1_data,
    sample_ready,
    sample_out,         // register zero is grabbed on every sample interrupt
    wo_regs             // write-only regs
);

input clk, rst_n;
input regwrite;
input [9:0] regwrite_addrs;
input [31:0] regwrite_data;
input regread0;
input regread1;
input [5:0] src0_addrs;
output [31:0] src0_data;
input [5:0] src1_addrs;
output [31:0] src1_data;
input sample_ready;
output [23:0] sample_out;
output [31:0] wo_regs[960];

logic [31:0] regs[1024]; // 10-bit addresses -> 1024 locations

assign sample_out = regs[0][31:8]; // register zero is grabbed on every sample interrupt

// input to register file
generate
genvar i;
for (i = 0; i < 1024; i = i + 1) begin : BLOCK1
    always @(posedge clk, negedge rst_n) begin
        if (~rst_n) regs[i] <= 32'h00000000;
        // else if (sample_ready && i == 0) regs[0] <= 0; // clear r0
        else if ((regwrite_addrs == i) && (regwrite)) regs[i] <= regwrite_data;
        else regs[i] <= regs[i];
    end
end
endgenerate

// outputs
assign src0_data = regread0 ? regs[src0_addrs] : 32'h59595959;
assign src1_data = regread1 ? regs[src1_addrs] : 32'h59595959;
assign wo_regs = regs[64:1023];

endmodule
