

module multiplier(A,B,Product);


input signed [31:0] A, B;
output signed [31:0] Product;


logic signed [63:0] pp;
assign pp = A*B;

/*assign Product = 
	(pp > {1'b0, {WIDTH-1{1'b1}}}) ? {1'b0, {WIDTH-1{1'b1}}} :
	($signed(pp) < $signed({1'b1, {WIDTH-1{1'b0}}})) ? {1'b1, {WIDTH-1{1'b0}}} :
	pp[39:8];*/
	

assign pos_sat = $signed(pp) > (2**39-1);
assign neg_sat = $signed(pp) < (-2**39);
assign Product = 
	pos_sat ? 32'h7fffffff :
	neg_sat ? 32'h80000000 :
	{pp[63], pp[38:8]};
	
/*
assign Product = 
	($signed(pp) > $signed(2**(WIDTH-1)-1)) ? (2**(WIDTH-1)-1) :
	($signed(pp) < $signed(-(2**WIDTH))) ? -(2**WIDTH) :
	pp[39:8];
*/

endmodule
