`timescale 1 ps / 1 ps
module dsp_core_tb();



logic clk, rst_n, sample_ready, prog, clear, shm_write;
logic [9:0] rom0_index, rom1_index, rom2_index, rom3_index; //these are useless outputs
logic [15:0] rom0_data, rom1_data, rom2_data, rom3_data;
logic [31:0] shm_from, shm_to;
logic [7:0] shm_addr;
logic [63:0] instr_data;
logic [4:0] peripheral_sel; 
logic [11:0] peripheral_in;
logic [23:0] sample_in, sample_out;

dsp_core iDUT(
    .clk(clk), 
	.rst_n(rst_n),

    // control signals
    .sample_ready(sample_ready),

        // program rom
    .prog(prog), 
	.clear(clear),
    .instr_data(instr_data),

        // data roms
     .rom0_index(rom0_index),
     .rom1_index(rom1_index),
     .rom2_index(rom2_index),
     .rom3_index(rom3_index),
     .rom0_data(rom0_data),
     .rom1_data(rom1_data),
     .rom2_data(rom2_data),
     .rom3_data(rom3_data),

        // shared memory
    .shm_from(shm_from),
    .shm_addr(shm_addr),
    .shm_write(shm_write),
    .shm_to(shm_to),

        // sample
    .sample_in(sample_in),
    .sample_out(sample_out),

    .peripheral_in(peripheral_in),
    .peripheral_sel(peripheral_sel)

);
 


initial begin
clk = 0; 
rst_n = 1;


@(posedge clk);

rst_n = 0;
sample_ready = 0; 
prog = 0;
clear = 0;

@(posedge clk);

rst_n = 1; 
instr_data = 64'b110110_0000000001_000000_000000_00000000000000000000000000001100_1_111; // read periph, dst r1, src0 r0, src1 r0, 
//
rom0_data = 16'h0000;
rom1_data = 16'h1111;
rom2_data = 16'h2222;
rom3_data = 16'h3333;


sample_in = 32'h76543210;

peripheral_in = 12'h777;


@(posedge clk);

clear = 1;

@(posedge clk);

clear = 0;

@(posedge clk);

prog = 1;
instr_data = 64'b_110010_0000000011_000001_000001_000000000000000000000100_00000000_1_111 ; //mov, dest 3, src0 = 1, src1 = 1, immediate = 4, imm = true, unconditional


@(posedge clk);

prog = 1;
instr_data = 64'b_110010_0000000010_000001_000001_000000000000000000000011_00000000_1_111 ; //move, dest 2, src0 = 1, src1 = 1, immediate = 3, imm = true, unconditional


@(posedge clk);

prog = 1;
instr_data = 64'b_000001_0000000000_000011_000010_000000000000000000000000_00000000_0_111 ; //multiply, dest 0, src0 = 3, src1 = 2, immediate = 0, imm = false, unconditional


@(posedge clk);

prog = 1;

instr_data = 64'b_000000_0000000000_000011_000010_000000000000000000000000_00000000_0_111 ; //NOP  , dest 0, src0 = 3, src1 = 2, immediate = 12, imm = false, unconditional

@(posedge clk);

prog = 0;

@(posedge clk);

sample_ready = 1; 

@(posedge clk);

sample_ready = 0; 

@(posedge clk);
@(posedge clk);
@(posedge clk);

@(posedge clk);
@(posedge clk);
@(posedge clk);

if(sample_out != 24'd12)begin
	$display("Multiply doesnt work, or NO OP");
end

@(posedge clk);

clear = 1;

@(posedge clk);

clear = 0;

@(posedge clk);

prog = 1;
instr_data = 64'b_110010_0000000011_000001_000001_000000000000000000010100_00000000_1_111 ; //mov, dest 3, src0 = 1, src1 = 1, immediate = 20, imm = true, unconditional


@(posedge clk);

prog = 1;
instr_data = 64'b_110010_0000000010_000001_000001_000000000000000000000011_00000000_1_111 ; //move, dest 2, src0 = 1, src1 = 1, immediate = 3, imm = true, unconditional


@(posedge clk);


prog = 1;

instr_data = 64'b_001010_0000000000_000011_000000_000000000000000000000000_00000011_1_111 ; //arith shift right, dest 0, src0 = 3, src1 = 2, immediate = 3, imm = true, unconditional

@(posedge clk);
prog = 0;

@(posedge clk);

sample_ready = 1; 

@(posedge clk);

sample_ready = 0; 

@(posedge clk);
@(posedge clk);
@(posedge clk);

@(posedge clk);
@(posedge clk);
@(posedge clk);

if(sample_out != (20 >> 3 ))begin
	$display(" arith Shift right doesnt work");
end


@(posedge clk);

clear = 1;

@(posedge clk);

clear = 0;

@(posedge clk);

prog = 1;
instr_data = 64'b_110010_0000000011_000001_000001_000000000000000000000100_00000000_1_111 ; //mov, dest 3, src0 = 1, src1 = 1, immediate = 4, imm = true, unconditional


@(posedge clk);

prog = 1;
instr_data = 64'b_110010_0000000010_000001_000001_000000000000000000000011_00000000_1_111 ; //move, dest 2, src0 = 1, src1 = 1, immediate = 3, imm = true, unconditional


@(posedge clk);


prog = 1;

instr_data = 64'b_010000_0000000000_000011_000010_000000000000000000000000_00000011_0_111 ; //greater than, dest 0, src0 = 3, src1 = 2, immediate = 3, imm = false, unconditional

@(posedge clk);


prog = 1;

instr_data = 64'b_000001_0000000000_000011_000010_000000000000000000000000_00000000_0_000 ; //multiply, dest 0, src0 = 3, src1 = 2, immediate = 0, imm = false, pred reg = 0

@(posedge clk);

prog = 0;

@(posedge clk);

sample_ready = 1; 

@(posedge clk);

sample_ready = 0; 

@(posedge clk);
@(posedge clk);
@(posedge clk);

@(posedge clk);
@(posedge clk);
@(posedge clk);

if(sample_out != 12)begin
	$display("greater than or predication doesnt work");
end




@(posedge clk);

clear = 1;

@(posedge clk);

clear = 0;

@(posedge clk);

prog = 1;
instr_data = 64'b_110010_0000000011_000001_000001_011110001110001011000100_00000000_1_111 ; //mov, dest 3, src0 = 1, src1 = 1, immediate = big + 4, imm = true, unconditional


@(posedge clk);

prog = 1;
instr_data = 64'b_110010_0000000010_000001_000001_011111110000000001110011_00000000_1_111 ; //move, dest 2, src0 = 1, src1 = 1, immediate = big + 3, imm = true, unconditional


@(posedge clk);


prog = 1;

instr_data = 64'b_000001_0000000000_000011_000010_000000000000000000000000_00000000_0_111 ; //multiply, dest 0, src0 = 3, src1 = 2, immediate = 0, imm = false, unconditional

@(posedge clk);


prog = 1;
instr_data = 64'b_010110_0000000000_000011_000000_000000000000000000000000_00000000_0_111 ; //test saturate, pred = 0, src0 = 3, src1 = 0, immediate = 0, imm = false, unconditional


@(posedge clk);
prog = 1;
instr_data = 64'b_010110_0000000000_000011_000000_100000000000000000000000_00000000_1_111 ; //test saturate, pred = 0, src0 = 3, src1 = 0, immediate = 0x80000000, imm = true, unconditional

@(posedge clk);
prog = 1;
instr_data = 64'b_010110_0000000000_000011_000000_011111111111111111111111_11111111_1_111 ; //test saturate, pred = 0, src0 = 3, src1 = 0, immediate = 0x7FFFFFFF, imm = true, unconditional

@(posedge clk);

prog = 1;
instr_data = 64'b_110010_0000000000_000001_000001_000000000000000000000011_00000000_1_000 ; //move, dest 0, src0 = 1, src1 = 1, immediate =  3, imm = true, pred reg = 0


@(posedge clk);


prog = 0;

@(posedge clk);

sample_ready = 1; 

@(posedge clk);

sample_ready = 0; 

@(posedge clk);
@(posedge clk);
@(posedge clk);

@(posedge clk);
@(posedge clk);
@(posedge clk);



if(sample_out != 3)begin
	$display("test saturate did not work");
end




@(posedge clk);

clear = 1;

@(posedge clk);

clear = 0;

@(posedge clk);
shm_from = 4;
prog = 1;
instr_data = 64'b_110010_0000000011_000001_000001_000000000000000000000100_00000000_1_111 ; //mov, dest 3, src0 = 1, src1 = 1, immediate = 4, imm = true, unconditional


@(posedge clk);

prog = 1;
instr_data = 64'b_110010_0000000010_000001_000001_000000000000000000000000_00000101_1_111 ; //move, dest 2, src0 = 1, src1 = 1, immediate = 3, imm = true, unconditional


@(posedge clk);


prog = 1;
instr_data = 64'b_110101_0000000000_000011_000010_000000000000000000000000_00000000_0_111 ; //shared mem store, dest 0, src0 = 3, src1 = 2, immediate = 0, imm = false, unconditional

@(posedge clk);



prog = 1;
instr_data = 64'b_110100_0000000000_000011_000010_000000000000000000000000_00000000_0_111 ; //shared mem load, dest 0, src0 = 3, src1 = 2, immediate = 0, imm = false, unconditional



@(posedge clk);


prog = 0;

@(posedge clk);

sample_ready = 1; 

@(posedge clk);

sample_ready = 0; 

@(posedge clk);
@(posedge clk);
@(posedge clk);

@(posedge clk);
@(posedge clk);
@(posedge clk);


@(posedge clk);
@(posedge clk);
@(posedge clk);



@(posedge clk);
@(posedge clk);
@(posedge clk);


@(posedge clk);
@(posedge clk);
@(posedge clk);

@(posedge clk);
@(posedge clk);
@(posedge clk);

@(posedge clk);
@(posedge clk);
@(posedge clk);
if(sample_out != 4)begin
	$display("shared mem load /store does not work");
end

//*

@(posedge clk);

clear = 1;

@(posedge clk);

clear = 0;

@(posedge clk);
prog = 1;
instr_data = 64'b_110010_0000000011_000001_000001_000000000000000000000100_00000000_1_111 ; //mov, dest 3, src0 = 1, src1 = 1, immediate = 4, imm = true, unconditional


@(posedge clk);

prog = 1;
instr_data = 64'b_110010_0000000010_000001_000001_000000000000000000000101_00000000_1_111 ; //move, dest 2, src0 = 1, src1 = 1, immediate = 5, imm = true, unconditional


@(posedge clk);


prog = 1;
instr_data = 64'b_110000_0000000000_000011_000010_000000000000000000000001_00000000_1_111 ; //input history load, dest 0, src0 = 3, src1 = 2, immediate = 1, imm = true, unconditional

@(posedge clk);


prog = 0;

@(posedge clk);

sample_ready = 1; 

@(posedge clk);

sample_ready = 0; 

@(posedge clk);
@(posedge clk);
@(posedge clk);

@(posedge clk);
@(posedge clk);
@(posedge clk);

if(sample_out != 24'h7654321)begin
	$display("load input history does not work");
end

//*/



$stop;
end



always #1 clk <= ~clk; 






endmodule 
