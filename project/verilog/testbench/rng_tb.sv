module rng_tb();

logic clk, rst_n, load;
logic [4:0] data, seed;
rng iDUT(clk,rst_n, seed, load, data);
			
integer i;
initial begin
clk = 0;
rst_n = 0;
seed = 4;
load = 0;
@(posedge clk);

rst_n = 1;

@(posedge clk);

load = 1;

@(posedge clk);

load = 0;

@(posedge clk);


for(i = 0; i < 10024; i = i + 1) begin
	@(posedge clk);
end


$stop;
end

always #5 clk <= ~clk;

endmodule