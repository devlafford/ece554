module multiplier_tb();


reg [31:0] A, B;
wire [31:0] Product;

multiplier mul(
    .A(A),
    .B(B),
    .Product(Product)
);

initial begin

    // 1x0
    A = 32'h000000_00;
    B = 32'h000001_00;
    #1 $display("%x x %x = %x", A[31:8], B[31:8], Product[31:8]);

    // 1x1
    A = 32'h000001_00;
    B = 32'h000001_00;
    #1 $display("%x x %x = %x", A[31:8], B[31:8], Product[31:8]);

    // 2x2
    A = 32'h000002_00;
    B = 32'h000002_00;
    #1 $display("%x x %x = %x", A[31:8], B[31:8], Product[31:8]);

    // 40 x 40
    A = 32'h000028_00;
    B = 32'h000028_00;
    #1 $display("%x x %x = %x", A[31:8], B[31:8], Product[31:8]);

    // high saturation
    A = 32'h7FFFFF_00;
    B = 32'h7FFFFF_00;
    #1 $display("%x x %x = %x", A[31:8], B[31:8], Product[31:8]);

    // low saturation
    A = 32'h800000_00;
    B = 32'h800000_00;
    #1 $display("%x x %x = %x", A[31:8], B[31:8], Product[31:8]);

end










endmodule
