module pitch_shifter_tb();
	logic inSampleReady;
	logic outSampleReady;
	logic shiftDirection;
	logic [23:0] sampleIn;
	logic clk;
	logic rst;
	logic [23:0] sampleOut;
	
	pitch_shifter ps(.*);
	
	always #10 clk <= ~clk;
	always #50 sampleIn <= sampleIn + 1;
	initial begin
	  sampleIn = 24'h0;
	  rst = 1'b1;
	  shiftDirection = 1'b1;
	  clk = 1'b0;
	  #20;
	  rst = 1'b0;
	  inSampleReady = 1'b1;
	  #500000;
	end

endmodule
