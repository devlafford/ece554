module alu_tb();
    reg clk;
    reg [5:0] op;
    reg [31:0] in1, in2;
    logic [31:0] alu_out;


    alu iDUT(op, in1, in2, alu_out);

    always #5 clk = ~clk;

    initial begin
        clk = 0;
        op = 6'b000010; // Add

        // Overflow
        @(negedge clk)
        in1 = 32'h7FFFFFFF;
        in2 = 32'h7FFFFFFF;
        @(posedge clk)
        $display("in1:%h in2:%h alu_out:%h", in1, in2, alu_out);
        if(alu_out != 32'h7fffffff)
            $stop();
        @(negedge clk)
        in1 = 32'h60000000;
        in2 = 32'h20000000;
        @(posedge clk)
        $display("in1:%h in2:%h alu_out:%h", in1, in2, alu_out);
        if(alu_out != 32'h7fffffff)
            $stop();

        // Underflow
        @(negedge clk)
        in1 = 32'h80000000;
        in2 = 32'h80000000;
        @(posedge clk)
        $display("in1:%h in2:%h alu_out:%h", in1, in2, alu_out);
        if(alu_out != 32'h80000000)
            $stop();


        // Basic add 2 positive numbers
        @(negedge clk)
        in1 = 32'h40000000;
        in2 = 32'h00000001;
        @(posedge clk)
        $display("in1:%h in2:%h alu_out:%h", in1, in2, alu_out);
        if(alu_out != 32'h40000001)
            $stop();

        // Basic add 2 negative numbers
        @(negedge clk)
        in1 = 32'hffffffff;
        in2 = 32'hffffffff;
        @(posedge clk)
        $display("in1:%h in2:%h alu_out:%h", in1, in2, alu_out);
        if(alu_out != 32'hfffffffe)
            $stop();

        @(negedge clk)
        in1 = 32'hffffff9c;
        in2 = 32'hffffff9c;
        @(posedge clk)
        $display("in1:%h in2:%h alu_out:%h", in1, in2, alu_out);
        if(alu_out != 32'hffffff38)
            $stop();

        //Subtract
        @(negedge clk)
        op = 6'b000011;
        in1 = 32'h40000000;
        in2 = -32'h40000000;
        @(posedge clk)
        $display("in1:%h in2:%h alu_out:%h", in1, in2, alu_out);
        if(alu_out != 32'h7fffffff)
        $stop();
        @(negedge clk)
        in1 = 32'h60000000;
        in2 = -32'h20000000;
        @(posedge clk)
        $display("in1:%h in2:%h alu_out:%h", in1, in2, alu_out);
        if(alu_out != 32'h7fffffff)
        $stop();
    
        @(negedge clk)
        in1 = 32'h80000000;
        in2 = -32'hffffffff;
        @(posedge clk)
        $display("in1:%h in2:%h alu_out:%h", in1, in2, alu_out);
        if(alu_out != 32'h80000000)
        $stop();
    
    
        @(negedge clk)
        in1 = 32'h40000000;
        in2 = -32'h00000001;
        @(posedge clk)
        $display("in1:%h in2:%h alu_out:%h", in1, in2, alu_out);
        if(alu_out != 32'h40000001)
        $stop();
    
        @(negedge clk)
        in1 = 32'hffffffff;
        in2 = -32'hffffffff;
        @(posedge clk)
        $display("in1:%h in2:%h alu_out:%h", in1, in2, alu_out);
        if(alu_out != 32'hfffffffe)
        $stop();
    

        @(negedge clk)
        op = 6'b001010;
        in1 = 32'h80000000;
        in2 = 5;
        @(posedge clk);
        $display("in1:%h in2:%h alu_out:%h", in1, in2, alu_out);
        if(alu_out != 32'hfc000000)
            $stop();

        @(negedge clk)
        in1 = 32'h40000000;
        @(posedge clk)
        $display("in1:%h in2:%h alu_out:%h", in1, in2, alu_out);
        if(alu_out != 32'h02000000)
            $stop();

        $stop();
    end


endmodule

