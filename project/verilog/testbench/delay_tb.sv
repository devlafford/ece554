module delay_tb();


reg clk, rst_n;
reg sample_ready;
logic lsample, rsample;

delay delayer(
    .clk(clk),
    .rst_n(rst_n),
    .sample_ready(sample_ready),
    .lsample(lsample),
    .rsample(rsample)
);







endmodule
