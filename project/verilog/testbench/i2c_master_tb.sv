module i2c_master_tb();

reg clk, rst_n;
reg [7:0] address;
reg [15:0] data;
reg transmit;
wire sda, sclk, ready;

i2c_master i2c(
    .clk(clk),
    .rst_n(rst_n),
    .address(address),
    .data(data),
    .transmit(transmit),
    .sda(sda),
    .sclk(sclk),
    .ready(ready)
);

always #10 clk = ~clk;

initial begin

    clk = 0;
    rst_n = 0;
    
    $monitor(sda);
    // Digital Audio Interface Format
    @(negedge clk);
    rst_n = 1;
    address = 8'b00110100;
    data = 16'b0000111000011111;
    transmit = 1;
    @(negedge clk);
    transmit = 0;
    @(negedge clk);

    repeat (40000) #10;
    $stop;

end



endmodule
