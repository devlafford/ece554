module mult_tb();



logic signed [31:0] A, B, P;
real Ar, Br, Pr;

multiplier mult(
	.A(A),
	.B(B),
	.Product(P)
);

/*
always_comb begin
	integer pp;
	A = $rtoi(Ar*256.0);
	B = $rtoi(Br*256.0);
	pp = P;
	Pr = P[31] ? -($itor(-pp)/$itor(256)) : $itor(pp)/$itor(256);
end
*/


always @(B) begin
	
end

/*
always @(Br) begin
	#1 $display("%g * %g = %g", Ar, Br, Pr);
end
*/

initial begin

A = 32'h00000200;
B = 32'h00000200;
#1;
$display("%d * %d = %d", $signed(A), $signed(B), $signed(P));
	$display("%x * %x = %x", $signed(A), $signed(B), $signed(P));

A = -32'h00000200;
B = 32'h00000200;
#1;
$display("%d * %d = %d", $signed(A), $signed(B), $signed(P));
	$display("%x * %x = %x", $signed(A), $signed(B), $signed(P));

A = 32'h00000200;
B = -32'h00000200;
#1;
$display("%d * %d = %d", $signed(A), $signed(B), $signed(P));
	$display("%x * %x = %x", $signed(A), $signed(B), $signed(P));

A = -32'h00000200;
B = -32'h00000200;
#1;
$display("%d * %d = %d", $signed(A), $signed(B), $signed(P));
	$display("%x * %x = %x", $signed(A), $signed(B), $signed(P));

A = 32'hFFFFFF00;
B = 32'hFFFFFF00;
#1;
$display("%d * %d = %d", $signed(A), $signed(B), $signed(P));
	$display("%x * %x = %x", $signed(A), $signed(B), $signed(P));


A = 32'h80000200;
B = 32'h80000400;
#1;
$display("%d * %d = %d", $signed(A), $signed(B), $signed(P));
	$display("%x * %x = %x", $signed(A), $signed(B), $signed(P));


A = 32'h70000200;
B = 32'h0000400;
#1;
$display("%d * %d = %d", $signed(A), $signed(B), $signed(P));
	$display("%x * %x = %x", $signed(A), $signed(B), $signed(P));


end



endmodule
