module fir_tb();
    parameter RADIX_POS = 8;
    parameter WIDTH = 32;
    parameter DEPTH = 16;

    reg clk, rst_n;

    logic put, clear;
    logic [WIDTH-1:0] coeffs[DEPTH];
    logic [WIDTH-1:0] new_sample, product, last_sample;
    int i;

    fir #(.RADIX_POS(RADIX_POS), .WIDTH(WIDTH), .DEPTH(DEPTH)
         ) iDUT(.clk(clk), .rst_n(rst_n),
            .put(put), .clear(clear), .coeffs(coeffs),      //inputs
            .new_sample(new_sample),                        //inputs
            .product(product), .last_sample(last_sample));  //outputs

    always #5 clk = ~clk;

    initial begin
        clk = 1'b0;
        rst_n = 1'b0;
        put = 1'b1;
        clear = 1'b0;
        
        new_sample = 32'h00000100;
        for(i = 0; i < 32; i = i + 1)
            coeffs[i] = 32'h00000140;

        @(negedge clk);
        rst_n = 1'b1;

        repeat(17) @(posedge clk);

        $stop();

    end


endmodule

