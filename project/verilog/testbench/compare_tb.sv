module compare_tb();


logic   [5:0]   opcode;
logic clk, rst_n, result;
logic [31:0]  operand0, operand1;

compare iDUT(	 opcode,
    operand0, 
    operand1, 
    clk, 
    rst_n, 
    result);



integer i;
initial begin
    clk = 0;
    operand0 = 50;
    operand1 = 100;
    rst_n = 0;
    opcode = 6'b010110; //saturate
    @(posedge clk);

    rst_n = 1;

    @(posedge clk);

    if(result)begin
        $display("saturate did not work");
        $stop;
    end

    @(posedge clk);

    operand1 = 32'h7fffffff;

    @(posedge clk);

    if(~result)begin
        $display("saturate did not work");
        $stop;
    end

    operand1 = 32'h10000000;

    @(posedge clk);

    if(~result)begin
        $display("saturate did not work");
        $stop;
    end



    operand1 = 32'h10001000;
    operand0 = 32'h10000000;

    @(posedge clk);

    if(result)begin
        $display("saturate did not work");
        $stop;
    end
    operand0 = operand1; 
    opcode = 6'b010010; //greater than or equal to

    @(posedge clk);

    if(~result)begin
        $display("greater than or equal to did not work");
        $stop;
    end

    operand0 = operand1 + 1; 
    @(posedge clk);

    if(~result)begin
        $display("greater than or equal to did not work");
        $stop;
    end

    opcode = 6'b010000; //greatrer than

    operand0 = 20;
    operand1 = 17; 

    @(posedge clk);

    if(~result)begin
        $display("greater than  did not work");
        $stop;
    end



    opcode = 6'b010011; //less than or equal to

    @(posedge clk);

    if(result)begin
        $display("less than or equal to did not work");
        $stop;
    end

    opcode = 6'b010101; //not equal to

    @(posedge clk);

    if(~result)begin
        $display("not equal to did not work");
        $stop;
    end


    $stop;
end

always #5 clk <= ~clk;


endmodule
