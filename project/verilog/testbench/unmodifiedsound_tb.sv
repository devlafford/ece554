`timescale 1 ps / 1 ps
module unmodifiedsound_tb();

reg clk, rst_n;
reg adcdat;
wire dacdat;
wire adclrck, daclrck;
wire xck;

unmodifiedsound ums(

        //////////// Audio //////////
        .AUD_ADCDAT(adcdat),     // input data
        .AUD_ADCLRCK(adclrck),    // input request pin
        .AUD_BCLK(bclk),       // bit clock
        .AUD_DACDAT(dacdat),     // output data
        .AUD_DACLRCK(daclrck),    // output request pin
        .AUD_XCK(xck),        // system clock
        
        .CLOCK_50(clk),

        .FPGA_I2C_SCLK(sclk),
        .FPGA_I2C_SDAT(sdat),


        .CLOCK2_50(),
        .CLOCK3_50(),
        .CLOCK4_50(),

        .HEX0(),
        .HEX1(),
        .HEX2(),
        .HEX3(),
        .HEX4(),
        .HEX5(),
        .KEY(rst_n),
        .SW(10'b0000000001)
);

always begin
    # 20000 clk = ~clk;
end

initial begin
    clk = 0;
    rst_n = 0;
    adcdat = 1;
    @(negedge clk);
    rst_n = 1;
    @(negedge clk);

    repeat (1000000) @(posedge clk);
    $stop;

end

endmodule
