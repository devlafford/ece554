module sine_tb();


logic  signed [15:0]  sinus;
logic clk, rst;
logic [23:0] fcw_0,fcw_1,fcw_2,fcw_3;
logic [1:0] osc_select;

sinus_gen iDUT(clk ,rst, fcw_0,fcw_1,fcw_2,fcw_3,osc_select, sinus  );
			
integer i;
initial begin
clk = 0;
fcw_0 = 50;
fcw_1 = 100;
fcw_2 = 500;
fcw_3 = 10000;
osc_select = 3;
rst = 0;

@(posedge clk);

rst = 1;

@(posedge clk);

rst = 0;

@(posedge clk);

for(i = 0; i < 10024; i = i + 1) begin
//osc_select = (osc_select + 1)%4;
@(posedge clk);
if(osc_select == 3)
$display("%d", sinus);/*
else if(osc_select == 1)
$display("\t%d", sinus);
else if(osc_select == 2)
$display("\t\t%d", sinus);
else if(osc_select == 3)
$display("\t\t\t%d", sinus);//*/
end

$stop;
end
always #5 clk <= ~clk;





endmodule