`timescale 1 ps / 1 ps
module pmsdsp_tb();

reg clk, rst_n;
reg adcdat;
wire dacdat;
wire adclrck, daclrck;
wire xck;
reg key1;

pmsdsp pms(

        // ADC //
        .ADC_CONVST(),
        .ADC_DIN(),
        .ADC_DOUT(),
        .ADC_SCLK(),

        //////////// Audio //////////
        .AUD_ADCDAT(adcdat),     // input data
        .AUD_ADCLRCK(adclrck),    // input request pin
        .AUD_BCLK(bclk),       // bit clock
        .AUD_DACDAT(dacdat),     // output data
        .AUD_DACLRCK(daclrck),    // output request pin
        .AUD_XCK(xck),        // system clock
        
        .CLOCK_50(clk),

        .FPGA_I2C_SCLK(sclk),
        .FPGA_I2C_SDAT(sdat),

        .CLOCK2_50(),
        .CLOCK3_50(),
        .CLOCK4_50(),

        .HEX0(),
        .HEX1(),
        .HEX2(),
        .HEX3(),
        .HEX4(),
        .HEX5(),
        .KEY({2'b00, key1, rst_n}),
        .LEDR(),
        .SW(10'b0000000001),
        .GPIO()
);

always begin
    # 20000 clk = ~clk;
end

initial begin
    clk = 0;
    rst_n = 0;
    adcdat = 1;
    key1 = 1;
    @(negedge clk);
    rst_n = 1;
    @(negedge clk);

/*
    // test spart -> core rom transfer
    // left channel header
    force pms.ctl.piface.sprt.rx_buffer.buffer[00] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[01] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[02] = 8'hFF;
    force pms.ctl.piface.sprt.rx_buffer.buffer[03] = 8'hFF;
    force pms.ctl.piface.sprt.rx_buffer.buffer[04] = 8'hFF;
    force pms.ctl.piface.sprt.rx_buffer.buffer[05] = 8'hFF;
    force pms.ctl.piface.sprt.rx_buffer.buffer[06] = 8'hFF;
    force pms.ctl.piface.sprt.rx_buffer.buffer[07] = 8'hFF;

    // r0 <- ihist
    force pms.ctl.piface.sprt.rx_buffer.buffer[08] = 8'h0f;
    force pms.ctl.piface.sprt.rx_buffer.buffer[09] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[10] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[11] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[12] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[13] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[14] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[15] = 8'hC0;

    // r0 <- ihist again
    force pms.ctl.piface.sprt.rx_buffer.buffer[16] = 8'h0f;
    force pms.ctl.piface.sprt.rx_buffer.buffer[17] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[18] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[19] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[20] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[21] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[22] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[23] = 8'hC0;

    // right channel header
    force pms.ctl.piface.sprt.rx_buffer.buffer[24] = 8'h01;
    force pms.ctl.piface.sprt.rx_buffer.buffer[25] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[26] = 8'hFF;
    force pms.ctl.piface.sprt.rx_buffer.buffer[27] = 8'hFF;
    force pms.ctl.piface.sprt.rx_buffer.buffer[28] = 8'hFF;
    force pms.ctl.piface.sprt.rx_buffer.buffer[29] = 8'hFF;
    force pms.ctl.piface.sprt.rx_buffer.buffer[30] = 8'hFF;
    force pms.ctl.piface.sprt.rx_buffer.buffer[31] = 8'hFF;

    // r0 <- ihist
    force pms.ctl.piface.sprt.rx_buffer.buffer[32] = 8'h0f;
    force pms.ctl.piface.sprt.rx_buffer.buffer[33] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[34] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[35] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[36] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[37] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[38] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[39] = 8'hC0;

    // r0 <- ihist again
    force pms.ctl.piface.sprt.rx_buffer.buffer[40] = 8'h0f;
    force pms.ctl.piface.sprt.rx_buffer.buffer[41] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[42] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[43] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[44] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[45] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[46] = 8'h00;
    force pms.ctl.piface.sprt.rx_buffer.buffer[47] = 8'hC0;

    // end program header
    force pms.ctl.piface.sprt.rx_buffer.buffer[48] = 8'hFF;
    force pms.ctl.piface.sprt.rx_buffer.buffer[49] = 8'hFF;
    force pms.ctl.piface.sprt.rx_buffer.buffer[50] = 8'hFF;
    force pms.ctl.piface.sprt.rx_buffer.buffer[51] = 8'hFF;
    force pms.ctl.piface.sprt.rx_buffer.buffer[52] = 8'hFF;
    force pms.ctl.piface.sprt.rx_buffer.buffer[53] = 8'hFF;
    force pms.ctl.piface.sprt.rx_buffer.buffer[54] = 8'hFF;
    force pms.ctl.piface.sprt.rx_buffer.buffer[55] = 8'hFF;

    force pms.ctl.piface.sprt.rx_buffer.produce = 56;

    #100
    release pms.ctl.piface.sprt.rx_buffer.buffer;
    release pms.ctl.piface.sprt.rx_buffer.produce;
*/

    repeat (20000) @(negedge clk);
    $stop;

end

endmodule

