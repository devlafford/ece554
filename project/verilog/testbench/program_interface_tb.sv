module program_interface_tb();

reg clk, rst_n;

wire piface_tx, piface_rx;
wire new_program;
wire lprog, rprog;
wire [2:0] romprog;
wire [63:0] instr_data;
program_interface piface(
    .clk(clk),
    .rst_n(rst_n),
    .txd(piface_tx),
    .rxd(piface_rx),
    .receiving_program(),
    .new_program(new_program),
    .lprog(lprog),
    .rprog(rprog),
    .romprog(romprog),
    .instr_data(instr_data)
);

reg pc_spart_iocs, pc_spart_iorw, pc_spart_tbr;
wire [7:0] pc_spart_data;
spart pc_spart(
    .clk(clk),
    .rst(rst_n),
    .iocs(pc_spart_iocs),
    .iorw(pc_spart_iorw),
    .rda(),
    .tbr(pc_spart_tbr),
    .ioaddr(2'b0),
    .databus(pc_spart_data),
    .txd(piface_rx),
    .rxd(piface_tx)
);

program_rom lrom(
    .clk(clk),
    .rst_n(rst_n),
    .reset(new_program),
    .listen(lprog),
    .instr_data(instr_data),
    .program_counter(),
    .instr_data_out()
);

program_rom rrom(
    .clk(clk),
    .rst_n(rst_n),
    .reset(new_program),
    .listen(rprog),
    .instr_data(instr_data),
    .program_counter(),
    .instr_data_out()
);

program_rom rom0(
    .clk(clk),
    .rst_n(rst_n),
    .reset(new_program),
    .listen(romprog == 3'b000),
    .instr_data(instr_data),
    .program_counter(),
    .instr_data_out()
);

program_rom rom1(
    .clk(clk),
    .rst_n(rst_n),
    .reset(new_program),
    .listen(romprog == 3'b001),
    .instr_data(instr_data),
    .program_counter(),
    .instr_data_out()
);

reg [7:0] pc_program [0:4095];
reg [15:0] counter;
reg inc_counter;
always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) counter <= 0;
    else if (inc_counter) counter <= counter + 1;
    else counter <= counter;
end


always #5 clk = ~clk;

initial begin

    $readmemh("../verilog/testbench/fakeprog.mem", pc_program, 0);
    rst_n = 0;
    clk = 0;
    inc_counter = 0;
    pc_spart_iocs = 0; // enable
    pc_spart_iorw = 0; // write
    @(negedge clk);
    rst_n = 1;
    @(negedge clk);

    $monitor(piface.destination);

    // start sending bytes to the spart
    while (counter < 240) begin // until end of program
        while (~pc_spart_tbr) begin
            @(negedge clk); // make sure there is room in buffer
        end

        inc_counter = 1;
        pc_spart_iocs = 1; // enable
        pc_spart_iorw = 0; // write
        @(negedge clk);
        inc_counter = 0;
        pc_spart_iocs = 0; // enable

    end

    repeat (1000000) @(negedge clk);
    $stop;


end

assign pc_spart_data = pc_program[counter];



endmodule
