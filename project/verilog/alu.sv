module sat_add(in0, in1, out);
    parameter SZ = 32;
	parameter MAX_POS = ~(1 << SZ-1);
	parameter MAX_NEG = 1 << SZ-1;
	
    input [SZ-1:0] in0, in1;
    output logic [SZ-1:0] out;
    logic [SZ-1:0] tmp;

    assign out = in0 + in1;
	
	//assign out = (tmp[SZ-1] & !in0[SZ-1] & !in1[SZ-1]) ? MAX_POS : 
	//			 (!tmp[SZ-1] & in0[SZ-1] & in1[SZ-1]) ? MAX_NEG :
	//			 tmp[SZ-1:0];
	
	/*always_comb begin
		case(tmp[SZ:SZ-1])
			2'b01 : out = MAX_POS;
			2'b10 : out = MAX_NEG;
			default : out = tmp[SZ-1:0];
		endcase
	end*/
    //assign out = tmp[SZ:SZ-1] == 2'b10 ? (1 << SZ-1) : tmp[SZ:SZ-1] == 2'b01 ? ~(1 << SZ-1) : tmp[SZ-1:0];
endmodule

module alu(op, in0, in1, out);
    parameter SZ = 32;
	parameter RADIX_POS = 16;

    input [5:0] op;
    input [SZ-1:0] in0, in1;
    output reg signed [SZ-1:0] out;

    logic [SZ-1:0] add_out, sub_out, mul_out;
    sat_add s1(in0, in1, add_out);
    sat_add s2(in0, ~in1 + 1, sub_out);
	
	multiplier mul(
		.A(in0),
		.B(in1),
		.Product(mul_out)
	);

    always_comb begin
        case(op[3:0])
            4'b0001: out = mul_out; //Insert multiply here
            4'b0010: out = add_out;
            4'b0011: out = sub_out;
            4'b0100: out = in0 & in1;
            4'b0101: out = in0 | in1;
            4'b0110: out = in0 ^ in1;
            4'b0111: out = ~in1;
            4'b1000: out = in0 << in1[31:8];
            4'b1001: out = in0 >> in1[31:8];
            4'b1010: out = $signed(in0) >>> $signed(in1[31:8]);
            default: out = in0;
        endcase
    end

endmodule

