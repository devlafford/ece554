module sine(
    input clk,
    input rst_n,
    input [23:0] fcw_0,
    input [23:0] fcw_1,
    input [23:0] fcw_2,
    input [23:0] fcw_3,
    output [9:0] rom0_index,
    output [9:0] rom1_index,
    output [9:0] rom2_index,
    output [9:0] rom3_index,
    input [15:0] rom0_data,
    input [15:0] rom1_data,
    input [15:0] rom2_data,
    input [15:0] rom3_data,
    input [1:0] osc_select,
    output logic signed [15:0] sinus
);

    reg [23:0] accu_0;
    reg [23:0] accu_1;
    reg [23:0] accu_2;
    reg [23:0] accu_3;

    reg [1:0] fdiv_cnt;
    wire accu_en;
    reg accu_msb_q;

//process for frequency divider
always@(posedge clk, negedge rst_n)
begin
    if(~rst_n) fdiv_cnt <= 0;
    else if(accu_en == 1'b1) fdiv_cnt <= 0;
    else fdiv_cnt <= fdiv_cnt +1;    
end

//logic for accu enable signal, resets also the frequency divider counter
assign accu_en = (fdiv_cnt == 2'd2) ? 1'b1 : 1'b0;

//process for phase accumulator
always@(posedge clk, negedge rst_n) begin
    if(~rst_n) begin    
        accu_0 <= 0;
        accu_1 <= 0;
        accu_2 <= 0;
        accu_3 <= 0;
    end
    else if(accu_en == 1'b1) begin
        if(osc_select == 2'b11) accu_3 <= accu_3 + fcw_3;
        else if(osc_select == 2'b10) accu_2 <= accu_2 + fcw_2;
        else if(osc_select == 2'b01) accu_1 <= accu_1 + fcw_1;
        else if(osc_select == 2'b00) accu_0 <= accu_0 + fcw_0;
    end
end

//10 msb's of the phase accumulator are used to index the sinewave lookup-table
assign rom0_index = accu_0[23:14];
assign rom1_index = accu_1[23:14];
assign rom2_index = accu_2[23:14];
assign rom3_index = accu_3[23:14];

//16-bit sine value from lookup table
assign sinus =  osc_select == 2'b11 ? rom3_data :
                osc_select == 2'b10 ? rom2_data :
                osc_select == 2'b01 ? rom1_data :
                                      rom0_data ;
endmodule
