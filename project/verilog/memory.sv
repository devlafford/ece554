module memory(
    clk, rst_n, write,
    write_addrs,
    write_ports,
    read_addrs,
    read_ports
);

parameter WORD_WIDTH = 32;
parameter ADDR_WIDTH = 8;
parameter READ_PORTS = 1;
parameter WRITE_PORTS = 1;

input clk, rst_n;
input write[WRITE_PORTS];
input [ADDR_WIDTH-1:0] write_addrs[WRITE_PORTS];
input [WORD_WIDTH-1:0] write_ports[WRITE_PORTS];
input [ADDR_WIDTH-1:0] read_addrs[READ_PORTS];
output logic [WORD_WIDTH-1:0] read_ports[READ_PORTS];

logic [WORD_WIDTH-1:0] mem[2**ADDR_WIDTH];

generate
genvar i;
int j;
for(i=0;i<READ_PORTS; i+=1) begin : MEM_BLOCK1
    assign read_ports[i] = mem[read_addrs[i]];
end

for(i=0; i<2**ADDR_WIDTH; i+=1) begin : MEM_BLOCK2
    always @(posedge clk, negedge rst_n) begin
        if(!rst_n) mem[i] = 0;
        else begin
            for(j=0; j<WRITE_PORTS; j+=1) begin
                if(write[j] && write_addrs[j] == i) mem[i] <= write_ports[j];
            end
        end
    end
end

endgenerate

endmodule
