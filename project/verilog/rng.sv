/*module fibonacci_lfsr_nbit
   #(parameter BITS = 5)
   (
    input             clk,
    input             rst_n,

    output reg [4:0] data
    );

   reg [4:0] data_next;
   always_comb begin
      data_next = data;
      repeat(BITS) begin
         data_next = {(data_next[4]^data_next[1]), data_next[4:1]};
      end
   end

   always_ff @(posedge clk or negedge rst_n) begin
      if(!rst_n)
         data <= 5'h1f;
      else
         data <= data_next;
   end

endmodule
*/


module rng
   
   (
    input             clk,
    input             rst_n,
	input  [4:0] seed,
	input load,
    output reg [4:0] data
    );


always@(posedge clk, negedge rst_n)
begin
if(~rst_n)
	data <= 5'd0;
else begin
	if(load)begin
		data <= seed; 
	end
	else begin
		data[0] <= data[4] ^ data[1];
		data[1] <= data[0];
		data[2] <= data[1];
		data[3] <= data[2];
		data[4] <= data[3];
	end
end
end







endmodule