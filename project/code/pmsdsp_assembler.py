#!/usr/bin/python3
from time import sleep
import string
from serial import Serial, EIGHTBITS, PARITY_NONE, STOPBITS_ONE
import argparse
import re
import ast
import struct
from enum import Enum, auto
from sys import exit
import binascii

BAUD_RATE = 19200; 


parser = argparse.ArgumentParser('Assemble and/or flash a PMSDSP assembly program')
parser.add_argument('srcfile', help='assembly or machine code source file')
parser.add_argument('-a', dest='assemble', action='store_true', default=False, help='assemble the given file', required=False)
#parser.add_argument('--wait', dest='wait', type=int, default=None, help='wait in between byte transmission', required=False)
parser.add_argument('-f', dest='flash', type=str, default=None, help='flash the given file or assembly result to given COM port', required=False)
parser.add_argument('-w', dest='write', type=argparse.FileType("wb"), default=None, help='store output to file', required=False)
parser.add_argument('-x', dest='hex', action="store_true", default=False, help='store output as hex', required=False)
args = parser.parse_args()



ENDIANNESS = "little"
SAMPLE_RATE = 96000
FCW_TO_HZ = 1 #TODO find this

class Instruction:
	def __init__(self, opcode, dest, src0, src1, imm, src_imm, pred):
		self.opcode = opcode
		self.dest = dest
		self.src0 = src0
		self.src1 = src1
		self.imm = imm
		self.src_imm = src_imm
		self.pred = pred
		
	def get_bytes(self):
		num = ((self.opcode&0x3F) << 58)
		num |= ((self.dest&0x3FF) << 48)
		num |= ((self.src0&0x3F) << 42)
		num |= ((self.src1&0x3F) << 36)
		num |= ((self.imm&0xFFFFFFFF) << 4)
		num |= ((self.src_imm & 0x01) << 3)
		num |= (self.pred&0x07)
		return num.to_bytes(8, byteorder=ENDIANNESS)
	
	
		
class OpcodeError(Exception):
	pass
	
class ArgumentError(Exception):
	pass
	
	
ARGEXPR_PATTERN = re.compile("^([a-z]+|[A-Z]+|#)([^\s]+)$")
	
#(?:[qQ][0-9]+(?:\.[0-9]+))|(?:0[xX][0-9a-fA-F]+)|(?:[0-9]+)
	
def extract_arg_value(prefixes, argstr):
	match = ARGEXPR_PATTERN.match(argstr)
	if not match:
		raise ArgumentError("Malformed argument expression for field: "+argstr)
	valid = False
	for p in prefixes:
		if p.upper() == match.group(1).upper():
			valid = True
			break
	if not valid:
		raise ArgumentError("Invalid argument expression prefix for field: "+argstr)
		
	val = None
	
	try:
		if match.group(2).upper()[0:2] == "0X":
			val = int(match.group(2), 16)
		elif match.group(2).upper()[0] == "Q":
			val = float(match.group(2)[1:])
			val *= 256
			val = int(round(val))
		elif match.group(2).upper()[-1] == "S":
			val = float(match.group(2)[:-1])
			val *= SAMPLE_RATE
			val = int(round(val))
		elif match.group(2).upper()[-2:] == "HZ":
			val = float(match.group(2)[:-2])
			val *= FCW_TO_HZ
			val = int(round(val))
		else:
			val = int(match.group(2))
	except ValueError:
		raise ArgumentError("Malformed number in argument expression: "+argstr)
		
	return val
	
def DEST(*prefixes):
	def _inner(inst, argstr):
		val = extract_arg_value(prefixes, argstr)
		inst.dest = val
	return _inner
	
def SRC0(*prefixes):
	def _inner(inst, argstr):
		val = extract_arg_value(prefixes, argstr)
		inst.src0 = val
	return _inner
	
def SRC1(inst, argstr):
	val = extract_arg_value(["#","r"], argstr)
	if argstr[0] == "#":
		inst.imm = val
		inst.src_imm = 1
	else:
		inst.src1 = val
		inst.src_imm = 0

def PRED(inst, argstr):
	val = extract_arg_value(["p"], argstr)
	inst.pred = val


DESTREG = DEST("r")
DESTPRED = DEST("p")
INST_DEFAULT = [DESTREG, SRC0("r"), SRC1] #required params for each instruction, in order
CMP_DEFAULT = [DESTPRED, SRC0("r"), SRC1]

INSTRUCTIONS = {
	"nop" : (0,[]),
	"mul" : (1,INST_DEFAULT),
	"add" : (2,INST_DEFAULT),
	"sub" : (3,INST_DEFAULT),
	"and" : (4,INST_DEFAULT),
	"orr" : (5,INST_DEFAULT),
	"xor" : (6,INST_DEFAULT),
	"not" : (7,[DESTREG, SRC1]), 
	"lsl" : (8,INST_DEFAULT),
	"lsr" : (9,INST_DEFAULT),
	"asr" : (10,INST_DEFAULT),
	
	"gt" : (16,CMP_DEFAULT),
	"lt" : (17,CMP_DEFAULT),
	"ge" : (18,CMP_DEFAULT),
	"le" : (19,CMP_DEFAULT),
	"eq" : (20,CMP_DEFAULT),
	"ne" : (21,CMP_DEFAULT),
	"ts" : (22,[DESTPRED, SRC1]),
	"lp" : (23,[DESTPRED]), # load predicates
	"sp" : (24,[SRC1]), # store predicates
	
	
	"oscr" : (32,[DESTREG, SRC0("osc")]), #osc read
	"firr" : (34,[DESTREG, SRC0("fir")]), #fir read
	"firp" : (35,[DESTREG, SRC0("fir"), SRC1]), #fir put
	"pshr" : (36,[DESTREG]), #pitch shift
	"pshp" : (37,[SRC1]), 
	"rngr" : (39,[DESTREG]), 
	
	#"oscs" : (33,[SRC0("osc"), SRC1]), #osc load
	#"pshs" : (41,[SRC1]), 
	#"rngs" : (40,[SRC1]), 
	
	
	"ihist" : (48,[DESTREG, SRC1]), #input history
	"ohist" : (49,[DESTREG, SRC1]), #output history
	"mov" : (50,[DESTREG, SRC1]), 
	
	"rdp" : (54,[DESTREG, SRC1]),
	
	"b" : (56,[SRC1]), #branch
	"l" : (57,[DESTREG]), #label (pc store)
	
	"hlt" : (62,[]),
}

#########################


def parse_instruction(inststr):
	
	icomps = inststr.split(" ")
	inst = Instruction(0,0,0,0,0,0,7);
	iargs = None
	
	if icomps[0] in INSTRUCTIONS:
		(inst.opcode, iargs) = INSTRUCTIONS[icomps[0]]
	else:
		raise OpcodeError("Invalid opcode mnemonic")
		
	if len(icomps)-1 < len(iargs):
		raise ArgumentError("Too few arguments given")
		
	for eval, argstr in zip(iargs+[PRED], icomps[1:]):
		eval(inst, argstr)
		
	return inst.get_bytes()
	
	
#(is this instruction memory?, rom header)
ROMS = {
	'LEFT': (True, 0xFFFFFFFF_FFFF0000.to_bytes(8, byteorder=ENDIANNESS)),
	'RIGHT': (True, 0xFFFFFFFF_FFFF0001.to_bytes(8, byteorder=ENDIANNESS)),
	'ROM0': (False, 0xFFFFFFFF_FFFF0100.to_bytes(8, byteorder=ENDIANNESS)),
	'ROM1': (False, 0xFFFFFFFF_FFFF0101.to_bytes(8, byteorder=ENDIANNESS)),
	'ROM2': (False, 0xFFFFFFFF_FFFF0102.to_bytes(8, byteorder=ENDIANNESS)),
	'ROM3': (False, 0xFFFFFFFF_FFFF0103.to_bytes(8, byteorder=ENDIANNESS)),
}

ROMLINE = re.compile("^([^\s:;]+):\s*(?:;.*)?$")
EMPTYLINE = re.compile("^\s*(?:;.*)?$")
CODELINE = re.compile("^([^\s;][^;]*)(?:;.*)?$")
HEXLINE = re.compile("^(0x[0-9a-fA-F]{1,16})\s*(?:;.*)?$")
ROM_CAPACITY = 1024

machinecode = []

flashable = True

if args.assemble:
	srcfile = open(args.srcfile, "r")
	
	rombytes = 0
	linecount = 0
	currrom = (False, b"")
	
	valid = True
	
	for line in srcfile:
		linecount += 1
		
		try:
			
			print("parsed line {}".format(line.rstrip()))
			
			match = HEXLINE.match(line)
			if match:
				#print("hexline")
				bytestr = ast.literal_eval(match.group(1)).to_bytes(8, byteorder=ENDIANNESS)
				machinecode.append(bytestr)
				rombytes += len(bytestr)
				continue
			
			match = EMPTYLINE.match(line)
			if match: 
				#print("empty line")
				continue
			
			match = ROMLINE.match(line)
			if match:
				#print("rom line")
				romname = match.group(1)
				if romname not in ROMS:
					raise Exception("Invalid ROM header")
					
				if not currrom[0]:
					if rombytes % 8 != 0:
						raise Exception("ROM contents not a multiple of 8 bytes")
						flashable = False
					
				rombytes = 0		
				currrom = ROMS[romname]
				machinecode.append(currrom[1])
				
				
				continue
				
			elif not currrom:
				raise Exception("File does not start with rom header")
			
			
			if currrom[0]:
				#print("instruction line")
				match = CODELINE.match(line.strip())
				if not match: raise Exception("Malformed Line")
				machinecode.append(parse_instruction(match.group(1).strip()))
			
			else:
				#print("rom data line")					
				try:
					byteline = ast.literal_eval(codestr.strip())
				except:
					raise Exception("Invalid rom data line")
				if type(byteline) is not bytes:
					raise Exception("Malformed rom line")
				rombytes += len(byteline)
				if rombytes > ROM_CAPACITY:
					raise Exception("Too much data loaded into rom")
				machinecode.append(rombytes)
				
		except Exception as e:
			flashable = False
			print("Error on line {} \"{}\"".format(linecount, line))
			print(str(e))
			raise e
			
			

	machinecode.append(b"\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF")
	
	fullmachinecode = bytearray()
	for b in machinecode:
		print(binascii.hexlify(bytearray(b)))
		#print(list(b))
		fullmachinecode += b
		
	machinecode = fullmachinecode
	
	srcfile.close()
	
else:
	srcfile = open(args.srcfile, "rb")
	machinecode = srcfile.read()
	srcfile.close()
	
if not flashable or len(machinecode) % 8 != 0:
	print("Program not valid. Terminating...")
	exit()
	
if args.flash:

	
	try:
		port = Serial(args.flash, BAUD_RATE, EIGHTBITS, PARITY_NONE, STOPBITS_ONE, timeout=10)
		
		
		port.write(machinecode)
		print("Flashing {} bytes".format(len(machinecode)))
		"""
		for byte in machinecode:
			port.write(byte)
			sleep(0.5)
			print(byte)
		"""
		port.flush()
		
		"""
		try:
			while True:
				print(make_readable(port.read()))
		except:
			pass #timeout*
		"""
	finally:
		port.close()

if args.write:
	if args.hex:
		for w in [machinecode[i*8:i*8+8] for i in range(len(machinecode)//8)]:
			hexstr = hex(struct.unpack("<Q", w)[0])[2:]
			hexstr = "0"*(16-len(hexstr)) + hexstr + "\n"
			args.write.write(hexstr.encode())
	else:
		args.write.write(machinecode)

