shift_reg_1024	shift_reg_1024_inst (
	.aclr ( aclr_sig ),
	.clken ( clken_sig ),
	.clock ( clock_sig ),
	.shiftin ( shiftin_sig ),
	.shiftout ( shiftout_sig ),
	.taps ( taps_sig )
	);
