//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:   
// Design Name: 
// Module Name:    spart 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module spart(
    input clk,
    input rst,
    input iocs,
    input iorw, // 0 - write, 1 - read
    output rda, // receive data available
    output tbr, // transmit buffer ready
    input [1:0] ioaddr,
    inout [7:0] databus,
    output txd,
    input rxd
    );

wire rst_n;
assign rst_n = rst;

// baud rate flops/wires
wire txshift, rxshift;
reg [15:0] divisor;
reg [15:0] txcounter;
reg [15:0] rxcounter;
assign txshift = ~|txcounter;
assign rxshift = ~|rxcounter;

// bus interface flops/wires
wire [7:0] data_in, data_out;

// tx flops/wires
reg [8:0] tx_shifter;
reg [3:0] tx_bit_count;

// rx flops/wires
reg [8:0] rx_shifter;
reg [3:0] rx_bit_count;

// txrx state machine
typedef enum reg {
    IDLE,
    ACTIVE
} state_t;
state_t txstate, txstate_next;
state_t rxstate, rxstate_next;
reg tx_load;
reg rx_start;

wire [7:0] tx_buffer_din;
reg [7:0] rx_buffer_din;
wire [7:0] tx_buffer_dout;
wire [7:0] rx_buffer_dout;
wire tx_buffer_empty;
wire rx_buffer_empty;
wire tx_buffer_full;
wire rx_buffer_full;

reg insert_tx, remove_tx;
buffer8 tx_buffer(
    .clk(clk),
    .rst_n(rst_n),
    .din(tx_buffer_din),
    .dout(tx_buffer_dout),
    .insert(insert_tx),
    .remove(remove_tx),
    .empty(tx_buffer_empty),
    .full(tx_buffer_full)
);

reg insert_rx, remove_rx;
buffer8 rx_buffer(
    .clk(clk),
    .rst_n(rst_n),
    .din(rx_buffer_din),
    .dout(rx_buffer_dout),
    .insert(insert_rx),
    .remove(remove_rx),
    .empty(rx_buffer_empty),
    .full(rx_buffer_full)
);

// INTERFACE AND CONTROL
reg read_status_cmd;
reg read_buf_cmd;
assign rda = !rx_buffer_empty;
assign tbr = !tx_buffer_full;
assign tx_buffer_din = databus;
assign insert_tx = iocs & (ioaddr == 0) & ~iorw;
assign remove_rx = read_buf_cmd;
assign databus = read_buf_cmd ? rx_buffer_dout : read_status_cmd ? {6'h00, tbr, rda} : 8'hZZ;
assign txd = tx_shifter[0];

// read commands need to return on the next clock
always_ff @(posedge clk, negedge rst_n) begin
    if (!rst_n) read_buf_cmd <= 0;
    else read_buf_cmd <= iocs & iorw & (ioaddr == 0);
end
always_ff @(posedge clk, negedge rst_n) begin
    if (!rst_n) read_status_cmd <= 0;
    else read_status_cmd <= iocs & iorw & (ioaddr == 1);
end

// baud rate generator
always_ff @(posedge clk, negedge rst_n) begin // tx baud counter reg
    if (!rst_n) begin
        txcounter <= 0;
    end

    // load the counter on start of activity
    else if (tx_load) txcounter <= divisor;

    // reload the counter after shifting a bit
    else if (txshift) txcounter <= divisor;

    // only burn power if actually doing something
    else if (txstate) txcounter <= txcounter - 1;

end

always_ff @(posedge clk, negedge rst_n) begin // rx baud counter reg
    if (!rst_n) begin
        rxcounter <= 0;
    end

    // load the counter on start of activity
    else if (rx_start) rxcounter <= divisor;

    // reload the counter after shifting a bit
    else if (rxshift) rxcounter <= divisor;

    // only burn power if actually doing something
    else if (rxstate) rxcounter <= rxcounter - 1;

end

always_ff @(posedge clk, negedge rst_n) begin // divisor reg
    if (!rst_n) begin
        divisor <= 0;
    end
    else if (iocs && ioaddr[1]) begin
        divisor <= ioaddr[0] ? {databus, divisor[7:0]} : {divisor[15:8], databus};
    end
    
end

// TRANSMIT

always_ff @(posedge clk, negedge rst_n) begin // bit counter
    if (!rst_n) begin
        tx_bit_count <= 0;
    end
    else if (tx_load) begin
        tx_bit_count <= 0;
    end
    else if (txshift && txstate) begin
        tx_bit_count <= tx_bit_count + 1;
    end
end

always_ff @(posedge clk, negedge rst_n) begin // shift reg
    if (!rst_n) begin
        tx_shifter <= 9'b111111111;
    end
    else if (tx_load) tx_shifter <= {tx_buffer_dout,1'b0};
    else if (txshift) tx_shifter <= {1'b1, tx_shifter[8:1]};
end

always_ff @(posedge clk, negedge rst_n) begin // state ff
    if (!rst_n) txstate <= IDLE;
    else txstate <= txstate_next;
end

always_comb begin // state transition logic
    
    tx_load = 0;
    remove_tx = 0;
    txstate_next = IDLE;

    case(txstate)
        IDLE: begin
            if (!tx_buffer_empty) begin
                txstate_next = ACTIVE;
                tx_load = 1;
                remove_tx = 1;
            end
        end

        ACTIVE: begin 
            if (tx_bit_count != 9) txstate_next = ACTIVE;
        end
    endcase
end

// RECEIVE
// double flop for metastability
reg rxdff1, rxdff2;
always_ff @(posedge clk, negedge rst_n) begin

    if (~rst_n) begin
        rxdff1 <= 1'b1;
        rxdff2 <= 1'b1;
    end
    else begin
        rxdff2 <= rxdff1;
        rxdff1 <= rxd;
    end
end

always_ff @(posedge clk, negedge rst_n) begin // bit counter
    if (!rst_n) begin
        rx_bit_count <= 0;
    end
    else if (rx_start) begin
        rx_bit_count <= 0;
    end
    else if (rxshift && rxstate) begin
        rx_bit_count <= rx_bit_count + 1;
    end
end

always_ff @(posedge clk, negedge rst_n) begin // shift reg
    if (!rst_n) begin
        rx_shifter <= 0;
    end
    else if (rxshift) rx_shifter <= {rxdff2, rx_shifter[8:1]};
end

always_ff @(posedge clk, negedge rst_n) begin // state ff
    if (!rst_n) rxstate <= IDLE;
    else rxstate <= rxstate_next;
end

always_comb begin // state transition logic
    
    rx_start = 0;
    rxstate_next = IDLE;
    rx_buffer_din = rx_shifter;
    insert_rx = 0;

    case(rxstate)
        IDLE: begin
            if (~rxdff2) begin
                rxstate_next = ACTIVE;
                rx_start = 1;
            end
        end

        ACTIVE: begin 
            if (rx_bit_count != 9) rxstate_next = ACTIVE;
            else insert_rx = 1;
        end
    endcase
end




endmodule


module buffer8(clk, rst_n, din, dout, insert, remove, empty, full);
    localparam BUFFER_DEPTH = 8; // this should always be a power of two

    input clk, rst_n;
    input [7:0] din;
    input insert;
    input remove;
    output [7:0] dout;
    output empty, full;

    reg [$clog2(BUFFER_DEPTH)-1:0] produce;
    reg [$clog2(BUFFER_DEPTH)-1:0] consume;
    reg [7:0] buffer [BUFFER_DEPTH:0];
    
    assign dout = buffer[consume];
    assign empty = consume == produce;
    assign full = consume-1 == produce;
    reg [31:0] i;

always_ff @(posedge clk, negedge rst_n) begin
    if (!rst_n) begin 
        produce <= 0;
        consume <= 0;
        // don't need to init buffers
        for (i = 0; i < BUFFER_DEPTH; i = i + 1) begin
            buffer[i] <= 8'bxxxxxxxx;
        end
    end
    else begin
        if (insert) begin
            buffer[produce] <= din;
            produce <= produce + 1;
        end
        if (remove) begin
            consume <= consume + 1;
        end
    end
end

endmodule

