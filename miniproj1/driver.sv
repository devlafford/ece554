//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    
// Design Name: 
// Module Name:    driver 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module driver(
    input clk,
    input rst,
    input [1:0] br_cfg,
    output iocs,
    output iorw,
    input rda,
    input tbr,
    output [1:0] ioaddr,
    inout [7:0] databus
    );

wire rst_n;
assign rst_n = rst;

reg [1:0] clock_counter; // use this clock instead
wire clock;
assign clock = clock_counter[1];
always_ff @(posedge clk) begin
    if (~rst_n) clock_counter <= 0;
    else clock_counter <= clock_counter + 1;
end


reg [15:0] baud;
reg [7:0] char;
wire [11:0] instr;
reg get_char;
reg have_char;
reg lowbaud_set;
reg highbaud_set;

wire [11:0] setlow, sethigh, read, write;

// Instruction format: DATA, IOADDR, IORW, IOCS
// set baud rate
assign setlow  = {baud[7:0],  2'b10, 1'b0, 1'b1};
assign sethigh = {baud[15:8], 2'b11, 1'b0, 1'b1};

// read a character
assign read =  {8'hZZ, 2'b00, 1'b1, 1'b1}; 

// send a character
assign write =  {char, 2'b00, 1'b0, 1'b1}; 

// assign signals based on instruction
assign databus = ~iorw & iocs? instr[11:4] : 8'bzzzzzzzz;
assign ioaddr = instr[3:2];
assign iorw = instr[1];
assign iocs = instr[0];

always_ff @(posedge clk, negedge rst_n) begin
    if (~rst_n) begin
        char <= 0;
        //instr <= 0;
        get_char <= 0;
        have_char <= 0;
        lowbaud_set <= 0;
        highbaud_set <= 0;

        case (br_cfg) // on reset, figure out what baud to use
            2'b00 : baud <= 325; // 4800 
            2'b01 : baud <= 162; // 9600
            2'b10 : baud <= 80; // 19200
            2'b11 : baud <= 40; // 38400
        endcase
    end

    else if (!lowbaud_set) begin
        lowbaud_set <= 1;
    end

    else if (!highbaud_set) begin
        highbaud_set <= 1;
    end

    else if (have_char && tbr) begin // send write command if we have a char and transmit ready
        //instr <= write;
        have_char <= 0;
    end

    else if (get_char) begin // cycle after sending the read command
        //instr <= 12'h000;
        char <= databus;
        get_char <= 0;
        have_char <= 1;
    end

    else if (rda & !have_char & !get_char) begin // if rda is high and we don't have a char and we haven't already requested a char, send read command
        //instr <= read;
        get_char <= 1;
    end

    else begin // do nothing
        //instr <= 12'h000;
    end

end

assign instr =  (!lowbaud_set) ? setlow :
                (!highbaud_set) ? sethigh :
                (have_char & tbr) ? write :
                (rda & !have_char & !get_char) ? read :
                12'h000;

endmodule
