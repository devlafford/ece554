module spart_tb();

wire txd;
wire rxd;

// spart0
wire iocs;
wire iorw;
wire rda;
wire tbr;
wire [1:0] ioaddr;
wire [7:0] databus;

// spart1
reg tb_iocs;
reg tb_iorw;
reg tb_rda;
reg tb_tbr;
reg [1:0] tb_ioaddr;
reg [7:0] tb_data;
wire [7:0] tb_databus;

wire [1:0] br_cfg;
assign br_cfg = 2'b11;

reg clk;
reg rst_n;

// Instantiate DUT here
spart driverspart(   .clk(clk),
                .rst(rst_n),
                .iocs(iocs),
                .iorw(iorw),
                .rda(rda),
                .tbr(tbr),
                .ioaddr(ioaddr),
                .databus(databus),
                .txd(txd),
                .rxd(rxd)
            );

driver driver( .clk(clk),
                .rst(rst_n),
                .br_cfg(br_cfg),
                .iocs(iocs),
                .iorw(iorw),
                .rda(rda),
                .tbr(tbr),
                .ioaddr(ioaddr),
                .databus(databus)
            );

// Instantiate spart used by tb here
spart tbspart(   .clk(clk),
                .rst(rst_n),
                .iocs(tb_iocs),
                .iorw(tb_iorw),
                .rda(tb_rda),
                .tbr(tb_tbr),
                .ioaddr(tb_ioaddr),
                .databus(tb_databus),
                .txd(rxd),
                .rxd(txd)
            );


always begin
    #20 clk = ~clk;
end

assign tb_databus = tb_iocs & ~tb_iorw ? tb_data : 8'hZZ;

initial begin
    rst_n = 0;
    clk = 0;

    @(negedge clk);
    rst_n = 1;

    // set baud rate
    tb_iocs = 1;
    tb_iorw = 0;
    tb_ioaddr = 10;
    tb_data = 8'h45; // 38400 low
    @(negedge clk);

    tb_iocs = 1;
    tb_iorw = 0;
    tb_ioaddr = 11;
    tb_data = 8'h01; // 38400 high
    @(negedge clk);

    // send hello
    tb_iocs = 1;
    tb_iorw = 0;
    tb_ioaddr = 00;
    tb_data = 8'h68; // h
    @(negedge clk);
    tb_iocs = 0;

    tb_iocs = 1;
    tb_iorw = 0;
    tb_ioaddr = 00;
    tb_data = 8'h65; // e
    @(negedge clk);
    tb_iocs = 0;

    tb_iocs = 1;
    tb_iorw = 0;
    tb_ioaddr = 00;
    tb_data = 8'h6C; // l
    @(negedge clk);
    tb_iocs = 0;

    tb_iocs = 1;
    tb_iorw = 0;
    tb_ioaddr = 00;
    tb_data = 8'h6C; // l
    @(negedge clk);
    tb_iocs = 0;

    tb_iocs = 1;
    tb_iorw = 0;
    tb_ioaddr = 00;
    tb_data = 8'h6F; // o
    @(negedge clk);
    tb_iocs = 0;

    // take a breather
    repeat (100) @(negedge clk);

    // read hello
    repeat (5) begin
        while (~tb_rda) begin
            @(negedge clk);
        end
        tb_iocs = 1;
        tb_iorw = 1;
        tb_ioaddr = 00;
        @(negedge clk);
        tb_iocs = 0;
        $display("%h", tb_databus);
        @(negedge clk);
    end

    $stop;





end

endmodule




