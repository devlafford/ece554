module grayscale_tb();

reg clk, rst_n;
reg rgb_ready;
reg [11:0] iRed, iGreen, iBlue;
wire [11:0] oGray;
wire oDVAL;

grayscale_buffer gb(
    .iCLK(clk),
    .iRST(rst_n),
    .rgb_ready(rgb_ready),
    .iRed(iRed),
    .iGreen(iGreen),
    .iBlue(iBlue),
    .oGray(oGray),
    .oDVAL(oDVAL)


);


always begin 
    # 5 clk = ~clk;
end

initial begin
    $monitor("oGray: %d oDVAL: %b", oGray, oDVAL);
    clk = 0;
    rst_n = 0;
    rgb_ready = 0;
    
    @(negedge clk);
    rst_n = 1;

    @(negedge clk);
    iRed = 3072;
    iGreen = 0000;
    iBlue = 0000;
    rgb_ready = 1;
    @(negedge clk);
    rgb_ready = 0;

    @(negedge clk);
    iRed = 3072;
    iGreen = 3072;
    iBlue = 0000;
    rgb_ready = 1;
    @(negedge clk);
    rgb_ready = 0;

    @(negedge clk);
    iRed = 3072;
    iGreen = 3072;
    iBlue = 3072;
    rgb_ready = 1;
    @(negedge clk);
    rgb_ready = 0;



    $stop;
end

endmodule
