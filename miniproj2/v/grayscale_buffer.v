module grayscale_buffer(
	input iCLK, iRST,
        input rgb_ready,
        input [11:0] iRed, iGreen, iBlue,
        output [11:0] oGray,
	output reg oDVAL
);

wire [14:0] sum;
assign sum = (iRed + iGreen + iBlue);
reg [15:0] gray_pixel;
assign oGray = gray_pixel;

always @(posedge iCLK, negedge iRST) begin
    if(~iRST) begin
        gray_pixel <= 12'h000; // black pixel
        oDVAL <= 1'b0;
    end
    else begin
        if (rgb_ready) begin // debayered pixel ready
            // gray_pixel <= (sum << 5) + (sum << 4) + (sum << 2); // 1/32 1/16 1/4 = * .344 avoid division
            gray_pixel <= sum / 3;
        end
        oDVAL <= rgb_ready;
    end
end

endmodule


