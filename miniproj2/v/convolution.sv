module convolution(
    input iCLK, iRST,
    input [11:0] iGray,
    input iDVAL,
    output [11:0] oRed, oBlue, oGreen,
    output reg oDVAL,
    input enable,
    input iSW
);

// oDVAL is one cycle after iDVAL
always_ff @(posedge iCLK) begin
    oDVAL <= iDVAL;
end

// column counter
reg [9:0] col_counter;
always_ff @(posedge iCLK, negedge iRST) begin
	if(!iRST) col_counter <= 0;
	else if(iDVAL && col_counter == 639) col_counter <= 0;
	else if(iDVAL) col_counter <= col_counter + 1'b1;
	else col_counter <= col_counter;
end

// very long shift register to hold two rows of pixels + the next three pixels
reg [11:0] pixels[1283]; // 640 * 2 + 3
generate
genvar x;
    for (x = 0; x < 1282; x++) begin : GENLOOP
        always_ff @(posedge iCLK, negedge iRST) begin
            if (~iRST) begin
                pixels[x] <= 12'h000;
            end
            else if (iDVAL) begin
                pixels[x] <= pixels[x+1];
            end
        end
    end
endgenerate

always_ff @(posedge iCLK, negedge iRST) begin // last pixel for above generate statement
    if (~iRST) begin
        pixels[1282] <= 12'h000;
    end
    else if (iDVAL) begin
        pixels[1282] <= iGray;
    end
end

// set the weights
parameter logic signed [7:0] vertical[3][3] = 
	'{
	 '{-1, 0, 1},
	 '{-2, 0, 2},
	 '{-1, 0, 1}
	};

parameter logic signed [7:0] horizontal[3][3] = 
	'{
	 '{-1, -2, -1},
	 '{0,  0,  0},
	 '{1,  2,  1}
	};

// select the pixels to be convolved
reg signed [11:0] kernel[3][3];
assign kernel[0][0] = (col_counter != 0) ? pixels[0] : 12'h000;
assign kernel[0][1] = (col_counter != 0) ? pixels[640] : 12'h000;
assign kernel[0][2] = (col_counter != 0) ? pixels[1280] : 12'h000;
assign kernel[1][0] = pixels[1];
assign kernel[1][1] = pixels[641];
assign kernel[1][2] = pixels[1281];
assign kernel[2][0] = (col_counter != 639) ? pixels[2] : 12'h000;
assign kernel[2][1] = (col_counter != 639) ? pixels[642] : 12'h000;
assign kernel[2][2] = (col_counter != 639) ? pixels[1282] : 12'h000;

// do the convolution
wire signed [31:0] vpixel;
wire signed [31:0] hpixel;
wire signed [11:0] pixel_out;
assign pixel_out = iSW ? hpixel[11:0] : vpixel[11:0];
assign vpixel = ( //possible bug, signedness and overflow
                kernel[0][0] * vertical[0][0] +
                kernel[0][1] * vertical[0][1] +
                kernel[0][2] * vertical[0][2] +
                kernel[1][0] * vertical[1][0] +
                kernel[1][1] * vertical[1][1] +
                kernel[1][2] * vertical[1][2] +
                kernel[2][0] * vertical[2][0] +
                kernel[2][1] * vertical[2][1] +
                kernel[2][2] * vertical[2][2]
		)
                /
                ((col_counter == 0 || col_counter == 639) ? 6 : 9);

assign hpixel = ( //possible bug, signedness and overflow
                kernel[0][0] * horizontal[0][0] +
                kernel[0][1] * horizontal[0][1] +
                kernel[0][2] * horizontal[0][2] +
                kernel[1][0] * horizontal[1][0] +
                kernel[1][1] * horizontal[1][1] +
                kernel[1][2] * horizontal[1][2] +
                kernel[2][0] * horizontal[2][0] +
                kernel[2][1] * horizontal[2][1] +
                kernel[2][2] * horizontal[2][2]
		)
                /
                ((col_counter == 0 || col_counter == 639) ? 6 : 9);


wire signed [11:0] abspixel;
assign abspixel = pixel_out[11] ? ~pixel_out + 1 : pixel_out;
assign {oRed, oGreen, oBlue} = enable ? {3{abspixel}} : {3{iGray}};

endmodule

