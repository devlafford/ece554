module proj2_tb();
/*
DE1_SoC_CAMERA iDUT(

      ///////// ADC /////////
                    ADC_CS_N,
                   ADC_DIN,
                    ADC_DOUT,
                   ADC_SCLK,

      ///////// AUD /////////
                    AUD_ADCDAT,
                    AUD_ADCLRCK,
                    AUD_BCLK,
                   AUD_DACDAT,
                    AUD_DACLRCK,
                   AUD_XCK,

      ///////// CLOCK2 /////////
                    CLOCK2_50,

      ///////// CLOCK3 /////////
                    CLOCK3_50,

      ///////// CLOCK4 /////////
                    CLOCK4_50,

      ///////// CLOCK /////////
                    CLOCK_50,

      ///////// DRAM /////////
            DRAM_ADDR,
              DRAM_BA,
                   DRAM_CAS_N,
                   DRAM_CKE,
                   DRAM_CLK,
                   DRAM_CS_N,
              DRAM_DQ,
                   DRAM_LDQM,
                   DRAM_RAS_N,
                   DRAM_UDQM,
                   DRAM_WE_N,

      ///////// FAN /////////
                   FAN_CTRL,

      ///////// FPGA /////////
                   FPGA_I2C_SCLK,
                    FPGA_I2C_SDAT,

      ///////// GPIO /////////
              GPIO_0,
	
      ///////// HEX0 /////////
              HEX0,

      ///////// HEX1 /////////
             HEX1,

      ///////// HEX2 /////////
             HEX2,

      ///////// HEX3 /////////
              HEX3,

      ///////// HEX4 /////////
              HEX4,

      ///////// HEX5 /////////
              HEX5,


      ///////// IRDA /////////
                    IRDA_RXD,
                   IRDA_TXD,

      ///////// KEY /////////
             KEY,

      ///////// LEDR /////////
              LEDR,

      ///////// PS2 /////////
                    PS2_CLK,
                    PS2_CLK2,
                    PS2_DAT,
                    PS2_DAT2,

      ///////// SW /////////
            SW,

      ///////// TD /////////
                    TD_CLK27,
              TD_DATA,
                    TD_HS,
                   TD_RESET_N,
                    TD_VS,

      ///////// VGA /////////
             VGA_B,
                   VGA_BLANK_N,
                   VGA_CLK,
              VGA_G,
                   VGA_HS,
              VGA_R,
                   VGA_SYNC_N,
                   VGA_VS,
		
		//////////// GPIO1, GPIO1 connect to D5M - 5M Pixel Camera //////////
	   		    D5M_D,
      		          D5M_FVAL,
      		          D5M_LVAL,
      		          D5M_PIXLCLK,
      		       D5M_RESET_N,
      		       D5M_SCLK,
      		          D5M_SDATA,
      		          D5M_STROBE,
      		       D5M_TRIGGER,
      		       D5M_XCLKIN
);
*/


logic clk, rst_n, capture, rgb_ready, oDVAL, line_ready;
logic [11:0] line_in[640];
logic [11:0] oRed, oGreen, oBlue;
integer i,j,k;

Convolution conv(
	line_in,//input [11:0] line_in[640],
	clk, rst_n, capture,//input
	rgb_ready,//output
	oRed, oGreen, oBlue,//[11:0] output
	oDVAL //tell memory to write
);

grayscale_buffer grey_buf( clk, rst_n,//input
oRed, oGreen, oBlue,//input [11:0] 
rgb_ready, //input
gray_pixels ,//output  [11:0] gray_pixels [0:639]
line_ready);//output


initial begin
	clk = 0;
	rst_n = 0;

	@(posedge clk);

	rst_n = 1;

	@(posedge clk);
	for(k = 0; k < 8 ; k=k+1)begin
		for(i = 0; i < 12 ; i=i+1)begin
			for(j = 0; j < 640 ; j=j+1)begin
				line_in[i][j] = $urandom%10;//needs to get assigned
			end
		end

	@(posedge clk);
	capture = 1'b1;
	@(posedge clk);
	capture = 1'b0;

	end
end

always begin
#5 clk <= ~clk;
end

endmodule
