module conv_tb();

reg clk, rst_n, capture, enable;
reg [11:0] conv_in;

wire [11:0] conv_r, conv_g, conv_b;
wire oDVAL;


// set filters to blur instead of edge detection for easier testing
convolution #(.vertical('{'{1, 1, 1},'{1, 1, 1},'{1, 1, 1}}), .horizontal('{'{1, 1, 1},'{1, 1, 1},'{1, 1, 1}}))
cv(
    .iCLK(clk),
    .iRST(rst_n),
    .iGray(conv_in),
    .iDVAL(capture),
    .oRed(conv_r),
    .oGreen(conv_g),
    .oBlue(conv_b),
    .oDVAL(oDVAL),
    .iSW(enable)
);
    
always begin 
	#5 clk = ~clk;
end

logic [32:0] i, j;
reg [11:0] expected;
initial begin
    clk = 0;
    rst_n = 0;
    enable = 0;
    capture = 0;
    conv_in = 0;
    i = 0;
    j = 0;
    repeat(3) @(posedge clk);
    rst_n = 1;
    capture = 1;
    begin // convolving with alternating vertical black and white bars
    	for(j = 0; j<10; j = j+1) begin
            for(i = 0; i<640; i = i + 1) begin
                if(j>2 && i > 1 && i < 638) begin
                    // we are expecting gray pixels since we are convolving with black and white bars
                    expected = enable ? (i%2 ? 12'h555 : 12'hAAA) : (i%2 ? 0 : -1); // 555 is dark gray AAA is light gray
                    if(expected != conv_r) begin
                        $display("expected %h out, got %h", expected, conv_r);
                        $stop;
                    end
                end
                conv_in = (i%2 ? -1 : 0);
                @(posedge clk);
            end
            $display("fed row %i", j);
        end
        enable = 1;
    end
   
    $stop;

end


endmodule
